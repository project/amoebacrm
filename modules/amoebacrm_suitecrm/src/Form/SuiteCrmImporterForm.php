<?php

namespace Drupal\amoebacrm_suitecrm\Form;

use Drupal\amoebacrm\Form\ImporterForm;
use Drupal\amoebacrm\Oauth2\RetrieveDataCrmInterface;
use Drupal\amoebacrm\Traits\EntityGenerateTrait;
use Drupal\amoebacrm_suitecrm\Grant\SuiteCrmProvider;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\State;

/**
 * Creates authorization form for SuiteCRM.
 */
class SuiteCrmImporterForm extends ImporterForm {
  use EntityGenerateTrait;

  const CRM_TYPE = 'suitecrm';

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, RetrieveDataCrmInterface $retrieveDataCrm, EntityManagerInterface $entityManager, State $state) {
    parent::__construct($config_factory, $retrieveDataCrm, $entityManager, $state);
    $this->retrieveDataCrm->setGrantType('password');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amoebacrm_suitecrm_importer';
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if (empty($this->requestConfig) || empty($this->requestConfig['username']) || empty($this->requestConfig['password'])) {
      $form_state->setErrorByName('import', $this->t('You need to first configure all Oauth2 credentials in order to use the importer.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->state->set('force_update', $form_state->getValue('force_update'));
    $this->retrieveDataCrm->setGrantTypeOptions(['username' => $this->requestConfig['username'], 'password' => $this->requestConfig['password']]);
    $retrievedData = $this->retrieveDataCrm->getDataFromCrm();
    if (is_array($retrievedData)) {
      $this->importEntities($form_state->get('entity_type_id'), $retrievedData);
    }
    else {
      $this->messenger()->addError('Something went wrong with the importer. Please check the logs for more information.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildProvider() {
    return new SuiteCrmProvider([
      'clientId' => $this->requestConfig['consumer_key'],
      'clientSecret' => $this->requestConfig['consumer_secret'],
      'domain' => $this->requestConfig['domain'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareContactMappingValues(array $responseData) {
    $entity = [];
    foreach ($responseData['data'] as $contact) {
      $companyId = self::entityLookup('company', 'name', $this->getCompanyFromContact($contact['id']));
      $countryCode = $this->getCountryCode($contact['attributes']['primary_address_country']);
      $address = [];
      if (!empty($countryCode)) {
        $address = [
          'address_line1' => !empty($contact['attributes']['primary_address_street']) ? $contact['attributes']['primary_address_street'] : '',
          'locality' => !empty($contact['attributes']['primary_address_city']) ? $contact['attributes']['primary_address_city'] : '',
          'state' => !empty($contact['attributes']['primary_address_state']) ? $contact['attributes']['primary_address_state'] : '',
          'zip_code' => !empty($contact['attributes']['primary_address_postalcode']) ? $contact['attributes']['primary_address_postalcode'] : '',
          'country_code' => !empty($countryCode) ? $countryCode : '',
        ];
      }
      $entity[] = [
        'title' => !empty($contact['attributes']['salutation']) ? $contact['attributes']['salutation'] : '',
        'first_name' => !empty($contact['attributes']['first_name']) ? $contact['attributes']['first_name'] : '',
        'last_name' => !empty($contact['attributes']['last_name']) ? $contact['attributes']['last_name'] : '',
        'company' => !empty($companyId) ? $companyId : '',
        'email' => $this->getEmailFromModule($contact['id'], 'Contact'),
        'mobile_phone_number' => !empty($contact['attributes']['phone_mobile']) ? $contact['attributes']['phone_mobile'] : '',
        'phone_number' => !empty($contact['attributes']['phone_work']) ? $contact['attributes']['phone_work'] : '',
        'fax' => !empty($contact['attributes']['phone_fax']) ? $contact['attributes']['phone_fax'] : '',
        'address' => !empty($address) ? $this->processAddressData($address) : '',
        'suitecrm_id' => !empty($contact['id']) ? $contact['id'] : '',
      ];
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareCompanyMappingValues(array $responseData) {
    $entities = [];
    foreach ($responseData['data'] as $company) {
      $industry = self::taxonomyGenerate($company['attributes']['industry'], 'industries');
      $countryCode = $this->getCountryCode($company['attributes']['billing_address_country']);
      $address = [];
      if (!empty($countryCode)) {
        $address = [
          'address_line1' => !empty($company['attributes']['billing_address_street']) ? $company['attributes']['billing_address_street'] : '',
          'locality' => !empty($company['attributes']['billing_address_city']) ? $company['attributes']['billing_address_city'] : '',
          'state' => !empty($company['attributes']['billing_address_state']) ? $company['attributes']['billing_address_state'] : '',
          'zip_code' => !empty($company['attributes']['billing_address_postalcode']) ? $company['attributes']['billing_address_postalcode'] : '',
          'country_code' => !empty($countryCode) ? $countryCode : '',
        ];
      }
      $entities[] = [
        'name' => !empty($company['attributes']['name']) ? $company['attributes']['name'] : '',
        'website' => $this->urlIsValid($company['attributes']['website']),
        'number_of_employees' => !empty($company['attributes']['employees']) ? $company['attributes']['employees'] : '',
        'email' => $this->getEmailFromModule($company['id'], 'Account'),
        'phone_number' => !empty($company['attributes']['phone_office']) ? $company['attributes']['phone_office'] : '',
        'fax' => !empty($company['attributes']['phone_fax']) ? $company['attributes']['phone_fax'] : '',
        'industry' => !empty($industry) ? $industry : '',
        'description' => !empty($company['attributes']['description']) ? $company['attributes']['description'] : '',
        'address' => !empty($address) ? $this->processAddressData($address) : '',
      ];
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEndpointPath($entityType = '') {
    if ($entityType == 'company') {
      $entityType = 'Account';
    }
    else {
      $entityType = 'Contact';
    }
    // Build the endpoint for retrieving data.
    $requestedFields = [
      'Account' => [
        'name',
        'website',
        'employees',
        'phone_office',
        'phone_fax',
        'industry',
        'description',
        'billing_address_street',
        'billing_address_city',
        'billing_address_state',
        'billing_address_postalcode',
        'billing_address_country',
      ],
      'Contact' => [
        'salutation',
        'first_name',
        'last_name',
        'email',
        'phone_mobile',
        'phone_work',
        'phone_fax',
        'primary_address_street',
        'primary_address_street_2',
        'primary_address_city',
        'primary_address_state',
        'primary_address_postalcode',
        'primary_address_country',
      ],
    ];
    $fieldsAsString = implode(',', $requestedFields[$entityType]);
    $fieldsQuery = [
      "fields[$entityType]" => $fieldsAsString,
    ];
    // @TODO check if it really works without 's'
    $moduleName = $entityType . 's';

    return '/Api/V8/module/' . $moduleName . '/?' . UrlHelper::buildQuery($fieldsQuery);
  }

  /**
   * Gets the email address from a Contact instance.
   *
   * @param string $contactId
   *   The id of the Contact requested.
   * @param string $moduleName
   *   The name of the module requested.
   *
   * @return string
   *   Returns the emails address if found, empty string otherwise.
   */
  public function getEmailFromModule($contactId, $moduleName) {
    $emailRelationship = $this->getModuleAttributes($moduleName, $contactId, 'email_addresses');
    if (!empty($emailRelationship['data'][0]['id'])) {
      $emailAddress = $this->getModuleAttributes('EmailAddresses', $emailRelationship['data'][0]['id']);
    }

    return !empty($emailAddress['data']['attributes']['email_address']) ? $emailAddress['data']['attributes']['email_address'] : '';
  }

  /**
   * Gets the company name from a Contact instance.
   *
   * @param string $contactId
   *   The id of the Contact requested.
   *
   * @return string
   *   Returns the company name if found, empty string otherwise.
   */
  public function getCompanyFromContact($contactId) {
    $emailRelationship = $this->getModuleAttributes('Contact', $contactId, 'accounts');
    if (!empty($emailRelationship['data'][0]['id'])) {
      $emailAddress = $this->getModuleAttributes('Account', $emailRelationship['data'][0]['id']);
    }

    return !empty($emailAddress['data']['attributes']['name']) ? $emailAddress['data']['attributes']['name'] : '';
  }

  /**
   * Gets attributes from module based on the entity id.
   *
   * @param string $moduleName
   *   The module name of entity requested.
   * @param string $moduleId
   *   The id of the module requested.
   * @param string $relationshipId
   *   The id of the relationship requested.
   *
   * @return mixed
   *   Returns the data requested if found, FALSE otherwise.
   */
  private function getModuleAttributes($moduleName, $moduleId, $relationshipId = '') {
    $isRelationship = !empty($relationshipId) ? "/relationships/$relationshipId" : '';
    $this->retrieveDataCrm->setEndpointPath("/Api/V8/module/$moduleName/$moduleId" . $isRelationship);

    return $this->retrieveDataCrm->getDataFromCrm();
  }

}
