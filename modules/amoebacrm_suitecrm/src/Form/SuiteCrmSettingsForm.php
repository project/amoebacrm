<?php

namespace Drupal\amoebacrm_suitecrm\Form;

use Drupal\amoebacrm\Form\ImporterSettingsForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for SuiteCrm importer.
 */
class SuiteCrmSettingsForm extends ImporterSettingsForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $crm_name = '') {
    $form = parent::buildForm($form, $form_state, $crm_name);
    $config = $this->config(static::SETTINGS)->get('request');

    $form['request'][$crm_name]['username'] = [
      '#title' => $this->t('Username'),
      '#type' => 'textfield',
      '#description' => $this->t('Enter the username of the SuiteCrm account.'),
      '#required' => TRUE,
      '#default_value' => isset($config[$crm_name]['username']) ? $config[$crm_name]['username'] : '',
    ];

    $form['request'][$crm_name]['password'] = [
      '#title' => $this->t('Password'),
      '#type' => 'password',
      '#description' => $this->t('Enter the password of the SuiteCrm account.'),
      '#required' => TRUE,
      '#default_value' => isset($config[$crm_name]['password']) ? $config[$crm_name]['password'] : '',
    ];

    return $form;
  }

}
