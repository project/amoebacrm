<?php

namespace Drupal\amoebacrm_suitecrm\Plugin\Action;

use Drupal\amoebacrm\Entity\ContactInterface;
use Drupal\amoebacrm_suitecrm\Services\SuiteCrmExporter;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Exports contacts in suitecrm.
 *
 * @Action(
 *   id = "export_to_suitecrm",
 *   label = @Translation("Export to suitecrm"),
 *   type = "actions_manager"
 * )
 */
class ExportToSuiteCrm extends ConfigurableActionBase implements ContainerFactoryPluginInterface {


  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The Suitecrm exporter.
   *
   * @var \Drupal\amoebacrm_suitecrm\Services\SuiteCrmExporter
   */
  protected $suiteCrmExporter;

  /**
   * Constructs an ExportSuiteCrm object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\amoebacrm_suitecrm\Services\SuiteCrmExporter $suitecrm_exporter
   *   The suitecrm exporter object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, SuiteCrmExporter $suitecrm_exporter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $logger;
    $this->suiteCrmExporter = $suitecrm_exporter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('logger.factory')->get('action'),
      $container->get('amoebacrm_suitecrm.contact_exporter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    if ($entity instanceof ContactInterface) {
      $exportContact = $this->suiteCrmExporter->exportContact($entity);
      if (is_array($exportContact) && !isset($exportContact['code'])) {
        $this->logger->notice('Contact exported to suitecrm');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::allowed();
    return $return_as_object ? $result : $result->isAllowed();
  }

}
