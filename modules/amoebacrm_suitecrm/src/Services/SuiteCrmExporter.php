<?php

namespace Drupal\amoebacrm_suitecrm\Services;

use Drupal\amoebacrm\Entity\ContactInterface;
use Drupal\amoebacrm\Oauth2\RetrieveDataCrm;
use Drupal\amoebacrm_suitecrm\Grant\SuiteCrmProvider;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides contact POST request functionality for SuiteCrm integration.
 */
class SuiteCrmExporter extends RetrieveDataCrm {

  use StringTranslationTrait;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The message log service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(Json $json, RequestStack $requestStack, StateInterface $state, ConfigFactoryInterface $config_factory, UrlGeneratorInterface $url_generator, MessengerInterface $messenger) {
    parent::__construct($json, $requestStack, $state);
    $this->configFactory = $config_factory;
    $this->urlGenerator = $url_generator;
    $this->messenger = $messenger;
    $this->initializeProvider();
  }

  /**
   * Exports the contact to SuiteCrm.
   *
   * @param \Drupal\amoebacrm\Entity\ContactInterface $entity
   *   The contact entity.
   */
  public function exportContact(ContactInterface $entity) {
    if (empty($this->getProvider())) {
      return [
        'code' => 401,
        'message' => 'Unauthorized, please check module configuration.',
      ];
    }
    // Make sure that the entity does not have a SuiteCrm id.
    $suiteCrmId = $entity->get('suitecrm_id')->getString();
    if (empty($suiteCrmId)) {
      $data = $this->mapEntityData($entity);
      if (!empty($data)) {
        $parameters = [
          'body' => $this->json->encode($data),
          'headers' => [
            'Content-Type' => 'application/vnd.api+json',
            'Accept' => 'application/vnd.api+json',
          ],
        ];
        $response = $this->getDataFromCrm('POST', $parameters);
        // Set the id of the contact created on SuiteCrm on the entity field.
        if (!empty($response) && !empty($response['data']['id'])) {
          $entity->set('suitecrm_id', $response['data']['id']);
          try {
            $entity->save();
          }
          catch (EntityStorageException $e) {
            watchdog_exception('SuiteCrmExporter', $e);
          }
        }
        return $response;
      }
    }
    else {
      // Update case.
      $data = $this->mapEntityData($entity);
      if (!empty($data)) {
        $parameters = [
          'body' => $this->json->encode($data),
          'headers' => [
            'Content-Type' => 'application/vnd.api+json',
            'Accept' => 'application/vnd.api+json',
          ],
        ];
        $response = $this->getDataFromCrm('PATCH', $parameters);

        return $response;
      }
    }

    return $entity;
  }

  /**
   * Map the entity field values with SuiteCrm parameters.
   *
   * @param \Drupal\amoebacrm\Entity\ContactInterface $entity
   *   The contact entity.
   *
   * @return array
   *   The mapped data.
   */
  protected function mapEntityData(ContactInterface $entity) {
    $address = $entity->get('address')->getValue();
    if (!empty($address)) {
      $address = reset($address);
    }

    $data = [
      "data" => [
        "type" => "Contacts",
        "attributes" => [
          "salutation" => $entity->getTitle(),
          "first_name" => $entity->get('first_name')->getString(),
          "last_name" => $entity->get('last_name')->getString(),
          "email" => $entity->get('email')->getString(),
          "phone_work" => $entity->get('phone_number')->getString(),
          "phone_mobile" => $entity->get('mobile_phone_number')->getString(),
          "primary_address_street" => !empty($address['address1']) ? $address['address1'] : '',
          "primary_address_street_2" => !empty($address['address2']) ? $address['address2'] : '',
          "primary_address_city" => !empty($address['locality']) ? $address['locality'] : '',
          "primary_address_state" => !empty($address['administrative_area']) ? $address['administrative_area'] : '',
          "primary_address_postalcode" => !empty($address['postal_code']) ? $address['postal_code'] : '',
          "primary_address_country" => !empty($address['country_code']) ? $address['country_code'] : '',
          "phone_fax" => $entity->get('fax')->getString(),
        ],
      ],
    ];
    $data["data"]["attributes"] = array_filter($data["data"]["attributes"]);

    if (!$entity->get('suitecrm_id')->isEmpty()) {
      // In case that the contact is already created in suitecrm we update
      // the data with it's id.
      $data["data"]["id"] = $entity->get("suitecrm_id")->getString();
    }

    return $data;
  }

  /**
   * Initializes and sets the provider.
   */
  public function initializeProvider() {
    $config = $this->configFactory->get('amoebacrm_importer.settings')->get('request');
    $requestConfig = isset($config['suitecrm']) ? $config['suitecrm'] : [];
    if (!empty($requestConfig['consumer_key']) && !empty($requestConfig['consumer_secret']) && !empty($requestConfig['domain']) &&
      !empty($requestConfig['username']) && !empty($requestConfig['password'])) {
      $this->setGrantType('password');
      $this->setGrantTypeOptions(['username' => $requestConfig['username'], 'password' => $requestConfig['password']]);
      $provider = new SuiteCrmProvider([
        'clientId' => $requestConfig['consumer_key'],
        'clientSecret' => $requestConfig['consumer_secret'],
        'domain' => $requestConfig['domain'],
      ]);
      $this->setEndpointUrl($requestConfig['domain']);
      $this->setEndpointPath('/Api/V8/module');
      $this->setProvider($provider);
    }
  }

}
