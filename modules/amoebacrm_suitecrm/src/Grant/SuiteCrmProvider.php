<?php

namespace Drupal\amoebacrm_suitecrm\Grant;

use League\OAuth2\Client\Provider\GenericProvider;

/**
 * Provider for SuiteCrm application.
 */
class SuiteCrmProvider extends GenericProvider {

  /**
   * The url of the SuiteCRM application.
   *
   * @var string
   */
  protected $domain;

  /**
   * {@inheritdoc}
   */
  protected function getConfigurableOptions() {
    return array_merge($this->getRequiredOptions(), [
      'accessTokenMethod',
      'scopeSeparator',
      'responseError',
      'responseCode',
      'scopes',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getRequiredOptions() {
    return [
      'domain',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseAccessTokenUrl(array $params) {
    return $this->domain . '/Api/access_token';
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseRetrieveDataUrl() {
    return $this->domain . '/Api/V8/module/Contacts';
  }

}
