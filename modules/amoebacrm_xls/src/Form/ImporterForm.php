<?php

namespace Drupal\amoebacrm_xls\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\amoebacrm_xls\ImporterBatchExecutable;
use Drupal\Core\Url;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\migrate\Plugin\MigrationPluginManager;

/**
 * Importer form.
 */
class ImporterForm extends FormBase {

  /**
   * The ID of the migrate file used to import CSV.
   *
   * @var string
   */
  protected $importerFileId;

  /**
   * The migration plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManager
   */
  protected $pluginManagerMigration;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The migration definitions.
   *
   * @var array
   */
  protected $definitions;

  /**
   * MigrateSourceUiForm constructor.
   *
   * @param \Drupal\migrate\Plugin\MigrationPluginManager $plugin_manager_migration
   *   The migration plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type plugin manager.
   */
  public function __construct(MigrationPluginManager $plugin_manager_migration, EntityTypeManagerInterface $entity_type_manager) {
    $this->pluginManagerMigration = $plugin_manager_migration;
    $this->definitions = $this->pluginManagerMigration->getDefinitions();
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.migration'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amoebacrm_xls_importer';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL) {
    $default_importers = [
      'company' => 'amoebacrm_xls_importer_company',
      'contact' => 'amoebacrm_xls_importer_contact',
    ];

    // Check if the entity id is allowed for import.
    if (!empty($default_importers[$entity_type_id]) && $entity_type = $this->entityTypeManager->getDefinition($entity_type_id)) {
      // Get the migrate plugin id from the configuration.
      $importer_csv_id = $this->config('amoebacrm_xls.adminsettings')->get($entity_type_id);
      $this->importerFileId = !empty($importer_csv_id) ? $importer_csv_id : $default_importers[$entity_type_id];

      // Get the example file for import.
      $example_file_path = drupal_get_path('module', 'amoebacrm_xls') . "/examples/$entity_type_id.csv";

      if (file_exists($example_file_path)) {
        $file_url = file_create_url($example_file_path);
        $example_file_text = $this->t('Example for the file headers structure');
        $form['helper_file'] = [
          '#title' => $this->t('Download'),
          '#type' => 'link',
          '#url' => Url::fromUri($file_url),
          '#prefix' => "<div>$example_file_text</div>",
        ];
      }

      $form['source_file'] = [
        '#type' => 'file',
        '#title' => $this->t('Upload an xls file that contains @entity_type', ['@entity_type' => $entity_type->getPluralLabel()]),
        '#description' => $this->t('Allowed types: @extensions.', ['@extensions' => 'csv xls xlsx']),
      ];

      $form['update_existing_records'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Update existing @entity_type', ['@entity_type' => $entity_type->getPluralLabel()]),
        '#default_value' => 1,
      ];

      $form['import'] = [
        '#type' => 'submit',
        '#value' => $this->t('Import'),
      ];
    }
    else {
      $form['missing_importer'] = [
        '#markup' => $this->t('The %entity_type entity is not supported by the importer.', ['%entity_type' => $entity_type_id]),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if (!empty($this->importerFileId)) {
      $validators = ['file_validate_extensions' => ['csv xls xlsx']];
      $file = file_save_upload('source_file', $validators, FALSE, 0);

      if (isset($file)) {
        // File upload was attempted.
        if ($file) {
          $form_state->setValue('file_path', $file->getFileUri());
        }
        // File upload failed.
        else {
          $form_state->setErrorByName('source_file', $this->t('The file could not be uploaded.'));
        }
      }
      else {
        $form_state->setErrorByName('source_file', $this->t('You have to upload a source file.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!empty($this->importerFileId)) {
      /** @var \Drupal\migrate\Plugin\MigrationInterface $migration */
      $migration = $this->pluginManagerMigration->createInstance($this->importerFileId);

      $options = [
        'file_path' => $form_state->getValue('file_path'),
      ];
      if ($form_state->getValue('update_existing_records')) {
        $options['update'] = TRUE;
      }
      try {
        $executable = new ImporterBatchExecutable($migration, new MigrateMessage(), $options);
        $executable->batchImport();
      }
      catch (MigrateException $e) {
      }
    }
  }

}
