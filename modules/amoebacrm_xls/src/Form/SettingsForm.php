<?php

namespace Drupal\amoebacrm_xls\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\migrate_spreadsheet\Plugin\migrate\source\Spreadsheet;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\migrate\Plugin\MigrationPluginManager;

/**
 * Settings form for entity importer configuration.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The migration plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManager
   */
  protected $pluginManagerMigration;

  /**
   * The migration definitions.
   *
   * @var array
   */
  protected $definitions;

  /**
   * MigrateSourceUiForm constructor.
   *
   * @param \Drupal\migrate\Plugin\MigrationPluginManager $plugin_manager_migration
   *   The migration plugin manager.
   */
  public function __construct(MigrationPluginManager $plugin_manager_migration) {
    $this->pluginManagerMigration = $plugin_manager_migration;
    $this->definitions = $this->pluginManagerMigration->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.migration')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'amoebacrm_xls.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amoebacrm_xls_importer_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the form values from config file.
    $config = $this->config('amoebacrm_xls.adminsettings');

    // Create an array with all the migration plugins of type Spreadsheet.
    foreach ($this->definitions as $definition) {
      $migrationInstance = $this->pluginManagerMigration->createStubMigration($definition);
      $migrationEntityType = $migrationInstance->getDestinationPlugin()->getPluginId();
      if ($migrationInstance->getSourcePlugin() instanceof Spreadsheet) {
        $options[$migrationEntityType][$definition['id']] = isset($definition['label']) ? $definition['label'] : $definition['id'];
      }
    }

    $form['company'] = [
      '#type' => 'select',
      '#title' => $this->t('Company importer file'),
      '#options' => !empty($options['entity:company']) ? $options['entity:company'] : [],
      '#default_value' => !empty($config->get('company')) ? $config->get('company') : 'amoebacrm_xls_importer_company',
    ];

    $form['contact'] = [
      '#type' => 'select',
      '#title' => $this->t('Contacts importer file'),
      '#options' => !empty($options['entity:contact']) ? $options['entity:contact'] : [],
      '#default_value' => !empty($config->get('contacts')) ? $config->get('contacts') : 'amoebacrm_xls_importer_contact',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('amoebacrm_xls.adminsettings')
      ->set('company', $form_state->getValue('company'))
      ->save();
    $this->config('amoebacrm_xls.adminsettings')
      ->set('contact', $form_state->getValue('contact'))
      ->save();
  }

}
