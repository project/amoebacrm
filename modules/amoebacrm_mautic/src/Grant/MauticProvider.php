<?php

namespace Drupal\amoebacrm_mautic\Grant;

use League\OAuth2\Client\Provider\GenericProvider;
use UnexpectedValueException;

/**
 * The Mautic Provider class.
 */
class MauticProvider extends GenericProvider {

  /**
   * The url of the Mautic application.
   *
   * @var string
   */
  protected $domain;

  /**
   * {@inheritdoc}
   */
  protected function getRequiredOptions() {
    return [
      'clientId',
      'clientSecret',
      'domain',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseAuthorizationUrl() {
    return $this->domain . '/oauth/v2/authorize';
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseAccessTokenUrl(array $params) {
    return $this->domain . '/oauth/v2/token';
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken($grant, array $options = []) {
    $grant = $this->verifyGrant($grant);

    $params = [
      'client_id'     => $this->clientId,
      'client_secret' => $this->clientSecret,
    ];
    if (!empty($this->redirectUri)) {
      $params['redirect_uri'] = $this->redirectUri;
    }
    $params   = $grant->prepareRequestParameters($params, $options);
    $request  = $this->getAccessTokenRequest($params);
    $response = $this->getParsedResponse($request);
    if (is_array($response) === FALSE) {
      throw new UnexpectedValueException(
        'Invalid response received from Authorization Server. Expected JSON.'
      );
    }
    $prepared = $this->prepareAccessTokenResponse($response);
    $token = $this->createAccessToken($prepared, $grant);

    return $token;
  }

}
