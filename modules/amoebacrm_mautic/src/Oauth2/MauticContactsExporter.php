<?php

namespace Drupal\amoebacrm_mautic\Oauth2;

use Drupal\amoebacrm\Entity\CompanyInterface;
use Drupal\amoebacrm\Entity\ContactInterface;
use Drupal\amoebacrm\Oauth2\RetrieveDataCrm;
use Drupal\amoebacrm_mautic\Grant\MauticProvider;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Locale\CountryManager;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides contact POST request functionality for Mautic integration.
 */
class MauticContactsExporter extends RetrieveDataCrm {

  use StringTranslationTrait;

  /**
   * The config factory property.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger factory property.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(Json $json, RequestStack $requestStack, StateInterface $state, ConfigFactoryInterface $config_factory, LoggerChannelFactory $loggerChannelFactory) {
    parent::__construct($json, $requestStack, $state);
    $this->configFactory = $config_factory;
    $this->logger = $loggerChannelFactory;
  }

  /**
   * Exports the contact to Mautic.
   *
   * @param \Drupal\amoebacrm\Entity\ContactInterface $entity
   *   The contact entity.
   */
  public function exportContact(ContactInterface $entity) {
    // Make sure that the entity does not have a Mautic id.
    $mauticId = $entity->get('mautic_id')->getString();
    if (empty($mauticId)) {
      $this->setGrantType('refresh_token');
      $state = $this->state->get('mautic');
      $this->setGrantTypeOptions(['refresh_token' => $state['refresh_token']]);
      $config = $this->configFactory->get('amoebacrm_mautic_importer.settings')->get('request');
      $providerValues = $config['mautic'];
      $provider = new MauticProvider([
        'clientId' => $providerValues['consumer_key'],
        'clientSecret' => $providerValues['consumer_secret'],
        'refresh_token' => $state['refresh_token'],
        'domain' => $providerValues['domain'],
      ]);
      $this->setProvider($provider);
      $response = $this->sendDataToCrm($entity, $providerValues, $state);
      // Set the id of the contact created on Mautic on the entity field.
      if (!empty($response) && !empty($response['contact'])) {
        $entity->set('mautic_id', $response['contact']['id']);
        try {
          $entity->save();
          $this->logger->get('amoebacrm_mautic')->info($this->t('Contact @contact exported successfully.', [
            '@contact' => $entity->label(),
          ]));
        }
        catch (EntityStorageException $e) {
          $this->logger->get('amoebacrm_mautic')->error($e->getMessage());
        }
      }
    }
  }

  /**
   * Map the entity field values with Mautic parameters.
   *
   * @param \Drupal\amoebacrm\Entity\ContactInterface $entity
   *   The contact entity.
   *
   * @return array
   *   The mapped data.
   */
  protected function mapEntityData(ContactInterface $entity) {
    $address = $entity->get('address')->getValue();
    if (!empty($address)) {
      $address = reset($address);
    }
    // Load the company object in order to have its name.
    $company = $entity->get('company')->referencedEntities();
    if (!empty($company)) {
      $company = reset($company);
    }
    $countries = CountryManager::getStandardList();
    $data = [
      "title" => $entity->getTitle(),
      "firstname" => $entity->get('first_name')->value,
      "lastname" => $entity->get('last_name')->value,
      "company" => $company instanceof CompanyInterface ? $company->getName() : '',
      "email" => $entity->get('email')->value,
      "phone" => $entity->get('phone_number')->value,
      "mobile" => $entity->get('mobile_phone_number')->value,
      "address1" => !empty($address['address1']) ? $address['address1'] : '',
      "address2" => !empty($address['address2']) ? $address['address2'] : '',
      "city" => !empty($address['locality']) ? $address['locality'] : '',
      "state" => !empty($address['administrative_area']) ? $address['administrative_area'] : '',
      "zipcode" => !empty($address['postal_code']) ? $address['postal_code'] : '',
      "country" => !empty($address['country_code']) && isset($countries[$address['country_code']]) ? $countries[$address['country_code']] : '',
      "fax" => $entity->get('fax')->value,
      "website" => $entity->get('website')->value,
      "facebook" => $entity->get('facebook')->value,
      "instagram" => $entity->get('instagram')->value,
      "linkedin" => $entity->get('linkedin')->value,
      "twitter" => $entity->get('twitter')->value,
    ];

    return $data;
  }

  /**
   * Sends contacts to the crm endpoint.
   *
   * @param \Drupal\amoebacrm\Entity\ContactInterface $entity
   *   The contact to be exported.
   * @param array $providerValues
   *   The domain of the CRM.
   * @param array $state
   *   The state values of the current CRM(access token, refresh token).
   *
   * @return bool|mixed
   *   Returns the request response, FALSE otherwise.
   */
  public function sendDataToCrm(ContactInterface $entity, array $providerValues, array $state) {
    try {
      $data = $this->mapEntityData($entity);
      $this->setGrantTypeOptions(['refresh_token' => $state['refresh_token']]);
      $parameters = [
        'body' => $this->json->encode($data),
      ];
      $this->setEndpointUrl($providerValues['domain'])->setEndpointPath('/api/contacts/new');
      $request = $this->provider->getAuthenticatedRequest('POST', $this->getRetrieveUrl(), $state['access_token'], $parameters);
      if (!empty($request)) {
        return $this->json->decode($this->provider->getResponse($request)->getBody()->getContents());
      }
    }
    catch (\Exception $e) {
      if ($e->getCode() === 401 && !empty($state['refresh_token'])) {
        $this->setGrantType('refresh_token');
        $provider = new MauticProvider([
          'clientId' => $providerValues['consumer_key'],
          'clientSecret' => $providerValues['consumer_secret'],
          'refresh_token' => $state['refresh_token'],
          'grant_type' => 'refresh_token',
          'domain' => $providerValues['domain'],
        ]);
        $this->setProvider($provider);
        $this->setGrantTypeOptions(['refresh_token' => $state['refresh_token']]);
        $accessToken = $this->storeAccessToken('mautic');
        if ($accessToken == FALSE) {
          $this->logger->get('amoebacrm_mautic')->error($this->t('Unable to generate new Mautic access token.'));
        }
        else {
          $this->exportContact($entity);
        }
      }
    }

    return FALSE;
  }

  /**
   * Stores the authorization data.
   *
   * @param string $crmName
   *   The CRM name to store data for.
   */
  public function storeAccessToken($crmName) {
    $code = $this->retrieveDataFromUrl('code');
    $state = $this->retrieveDataFromUrl('state');
    if ($this->getGrantType() == 'authorization_code') {
      if (empty($code)) {
        // Authorize the client.
        $authorizationUrl = $this->provider->getAuthorizationUrl();
        // Save the state of the provider.
        $this->state->set('oauthState', $this->provider->getState());
        $redirect = new RedirectResponse($authorizationUrl);
        $redirect->send();
        exit();
      }
      // Check given state against previously stored to mitigate CSRF attack.
      elseif (empty($state) || !empty($this->state->get('oauthState')) && $state !== $this->state->get('oauthState')) {
        // Delete the state from storage.
        if (!empty($this->state->get('oauthState'))) {
          $this->state->delete('oauthState');
        }
      }
      $this->setGrantTypeOptions(['code' => $code]);
    }

    $accessToken = $this->requestAccessToken();
    $this->state->set($crmName, [
      'access_token' => $accessToken->getToken(),
      'refresh_token' => $accessToken->getRefreshToken(),
      'expires' => $accessToken->getExpires(),
    ]);

    return !empty($accessToken) ? TRUE : FALSE;
  }

}
