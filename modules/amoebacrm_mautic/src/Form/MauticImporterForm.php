<?php

namespace Drupal\amoebacrm_mautic\Form;

use Drupal\amoebacrm\Form\ImporterForm;
use Drupal\amoebacrm\Traits\EntityGenerateTrait;
use Drupal\amoebacrm_mautic\Grant\MauticProvider;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Creates import form for MauticCRM.
 */
class MauticImporterForm extends ImporterForm {
  use EntityGenerateTrait;

  const CRM_TYPE = 'mautic';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mautic_importer';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL) {
    $form = parent::buildForm($form, $form_state, $entity_type_id);

    // Get the code from request.
    $code = $this->getRequest()->get('code');
    if (!empty($code)) {
      // Retrieve the data from the current CRM.
      $retrievedData = $this->retrieveDataCrm->getDataFromCrm();
      if (is_array($retrievedData)) {
        $this->importEntities($entity_type_id, $retrievedData);
      }
      $redirect = new RedirectResponse($this->getCurrentPageUrl());
      $redirect->send();
      exit();
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildProvider() {
    return new MauticProvider([
      'clientId' => $this->requestConfig['consumer_key'],
      'clientSecret' => $this->requestConfig['consumer_secret'],
      'redirectUri' => $this->getCurrentPageUrl(),
      'domain' => $this->requestConfig['domain'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareContactMappingValues(array $responseData) {
    $values = [];
    foreach ($responseData['contacts'] as $contact) {
      $companyId = self::entityLookup('company', 'name', $contact['fields']['all']['company']);
      $countryCode = $this->getCountryCode($contact['fields']['all']['country']);
      if (!empty($countryCode)) {
        $address = [
          'address_line1' => !empty($contact['fields']['all']['address1']) ? $contact['fields']['all']['address1'] : '',
          'address_line2' => !empty($contact['fields']['all']['address2']) ? $contact['fields']['all']['address2'] : '',
          'locality' => !empty($contact['fields']['all']['city']) ? $contact['fields']['all']['city'] : '',
          'state' => !empty($contact['fields']['all']['state']) ? $contact['fields']['all']['state'] : '',
          'zip_code' => !empty($contact['fields']['all']['zipcode']) ? $contact['fields']['all']['zipcode'] : '',
          'country_code' => !empty($countryCode) ? $countryCode : '',
        ];
      }
      $values[] = [
        'title' => !empty($contact['fields']['all']['title']) ? $contact['fields']['all']['title'] : '',
        'first_name' => !empty($contact['fields']['all']['firstname']) ? $contact['fields']['all']['firstname'] : '',
        'last_name' => !empty($contact['fields']['all']['lastname']) ? $contact['fields']['all']['lastname'] : '',
        'company' => !empty($companyId) ? $companyId : '',
        'email' => !empty($contact['fields']['all']['email']) ? $contact['fields']['all']['email'] : '',
        'mobile_phone_number' => !empty($contact['fields']['all']['mobile']) ? $contact['fields']['all']['mobile'] : '',
        'phone_number' => !empty($contact['fields']['all']['phone']) ? $contact['fields']['all']['phone'] : '',
        'fax' => !empty($contact['fields']['all']['fax']) ? $contact['fields']['all']['fax'] : '',
        'address' => !empty($address) ? $this->processAddressData($address) : '',
        'website' => $this->urlIsValid($contact['fields']['all']['website']),
        'facebook' => $this->urlIsValid($contact['fields']['all']['facebook']),
        'instagram' => $this->urlIsValid($contact['fields']['all']['instagram']),
        'linkedin' => $this->urlIsValid($contact['fields']['all']['linkedin']),
        'twitter' => $this->urlIsValid($contact['fields']['all']['twitter']),
      ];
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareCompanyMappingValues(array $responseData) {
    $values = [];
    foreach ($responseData['companies'] as $company) {
      $industryId = self::taxonomyGenerate($company['fields']['all']['companyindustry'], 'industries');
      $countryCode = $this->getCountryCode($company['fields']['all']['companycountry']);
      if (!empty($countryCode)) {
        $address = [
          'address_line1' => !empty($company['fields']['all']['companyaddress1']) ? $company['fields']['all']['companyaddress1'] : '',
          'address_line2' => !empty($company['fields']['all']['companyaddress2']) ? $company['fields']['all']['companyaddress2'] : '',
          'locality' => !empty($company['fields']['all']['companycity']) ? $company['fields']['all']['companycity'] : '',
          'state' => !empty($company['fields']['all']['companystate']) ? $company['fields']['all']['companystate'] : '',
          'zip_code' => !empty($company['fields']['all']['companyzipcode']) ? $company['fields']['all']['companyzipcode'] : '',
          'country_code' => !empty($countryCode) ? $countryCode : '',
        ];
      }
      $values[] = [
        'name' => !empty($company['fields']['all']['companyname']) ? $company['fields']['all']['companyname'] : '',
        'email' => !empty($company['fields']['all']['companyemail']) ? $company['fields']['all']['companyemail'] : '',
        'address' => !empty($countryCode) ? $this->processAddressData($address) : '',
        'phone_number' => !empty($company['fields']['all']['companyphone']) ? $company['fields']['all']['companyphone'] : '',
        'fax' => !empty($company['fields']['all']['companyfax']) ? $company['fields']['all']['companyfax'] : '',
        'website' => $this->urlIsValid($company['fields']['all']['companywebsite']),
        'number_of_employees' => !empty($company['fields']['all']['companynumber_of_employees']) ? $company['fields']['all']['companynumber_of_employees'] : '',
        'annual_revenue' => !empty($company['fields']['all']['companyannual_revenue']) ? $company['fields']['all']['companyannual_revenue'] : '',
        'industry' => !empty($industryId) ? $industryId : '',
        'description' => !empty($company['fields']['all']['companydescription']) ? $company['fields']['all']['companydescription'] : '',
      ];
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEndpointPath($entityType = '') {
    return $entityType == 'contact' ? '/api/contacts' : '/api/companies';
  }

}
