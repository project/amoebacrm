<?php

namespace Drupal\amoebacrm_mautic\Form;

use Drupal\amoebacrm\Form\ImporterSettingsForm;
use Drupal\amoebacrm_mautic\Grant\MauticProvider;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class MauticImportSettingsForm.
 *
 * @package Drupal\amoebacrm_mautic\Form
 */
class MauticImportSettingsForm extends ImporterSettingsForm {

  /**
   * The retrieve data crm service.
   *
   * @var \Drupal\amoebacrm_mautic\Oauth2\MauticContactsExporter
   */
  protected $retrieveDataCrm;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->retrieveDataCrm = $container->get('amoebacrm_mautic.contact_exporter');
    return $instance;
  }

  const SETTINGS = 'amoebacrm_mautic_importer.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amoeba_mautic_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $crm_name = '') {
    $this->messenger()->addWarning($this->t('Make sure that the refresh token lifetime is at least one day longer than the access token.'));
    $config = $this->config(static::SETTINGS)->get('request');
    $code = \Drupal::request()->get('code');
    if (!empty($code)) {
      $accessToken = $this->authorizeUser($config[$crm_name], $crm_name);
      if (!$accessToken) {
        $this->messenger()->addError($this->t('Access token could not be retrieved.'));
      }
      else {
        $this->messenger()->addMessage($this->t('Access token created successfully.'));
      }
      $redirect = new RedirectResponse(\Drupal::urlGenerator()->generateFromRoute('<current>'));
      $redirect->send();
    }
    return parent::buildForm($form, $form_state, $crm_name);
  }

  /**
   * {@inheritdoc}
   */
  protected function getProvider(array $providerValues = []) {
    return new MauticProvider([
      'clientId' => $providerValues['consumer_key'],
      'clientSecret' => $providerValues['consumer_secret'],
      'redirectUri' => \Drupal::urlGenerator()->generateFromRoute('<current>', [], ['absolute' => TRUE]),
      'domain' => $providerValues['domain'],
    ]);
  }

  /**
   * Save the tokens retrieved from the request.
   *
   * @param array $providerValues
   *   The provider values.
   * @param string $crmName
   *   The current CRM name.
   *
   * @return bool
   *   Returns TRUE if we have a access token, FALSE otherwise.
   */
  protected function authorizeUser(array $providerValues, $crmName) {
    $provider = $this->getProvider($providerValues);
    $this->retrieveDataCrm->setProvider($provider);
    // Set credentials.
    $this->retrieveDataCrm->setCredentials($providerValues);
    // Set default grant type.
    $this->retrieveDataCrm->setGrantType('authorization_code');
    return $this->retrieveDataCrm->storeAccessToken($crmName);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $crm_name = $form_state->getValue('crm_name');
    $providerValues = $form_state->getValue(['request', $crm_name]);

    /** @var \Drupal\amoebacrm_mautic\Oauth2\MauticContactsExporter $retrieveDataCrm */
    $retrieveDataCrm = \Drupal::service('amoebacrm_mautic.contact_exporter');
    $provider = $this->getProvider($providerValues);
    $retrieveDataCrm->setProvider($provider);
    // Set credentials.
    $retrieveDataCrm->setCredentials($providerValues);
    // Set default grant type.
    $retrieveDataCrm->setGrantType('authorization_code');
    $retrieveDataCrm->storeAccessToken($crm_name);
  }

}
