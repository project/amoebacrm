<?php

namespace Drupal\amoebacrm_mautic\Plugin\Action;

use Drupal\amoebacrm\Entity\ContactInterface;
use Drupal\amoebacrm_mautic\Oauth2\MauticContactsExporter;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Create contacts on Mautic.
 *
 * @Action(
 *   id = "mautic_export",
 *   label = @Translation("Create contact on Mautic"),
 *   type = "actions_manager"
 * )
 */
class MauticExport extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  /**
   * The Mautic exporter service property.
   *
   * @var \Drupal\amoebacrm_mautic\Oauth2\MauticContactsExporter
   */
  protected $mauticExporter;

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $result = AccessResult::allowed();
    return $return_as_object ? $result : $result->isAllowed();
  }

  /**
   * MauticExport constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\amoebacrm_mautic\Oauth2\MauticContactsExporter $mauticExporter
   *   The Mautic exporter service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MauticContactsExporter $mauticExporter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->mauticExporter = $mauticExporter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('amoebacrm_mautic.contact_exporter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    if ($entity instanceof ContactInterface) {
      $this->mauticExporter->exportContact($entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state){}

}
