<?php

namespace Drupal\amoebacrm_salesforce\Services;

use Drupal\amoebacrm\Entity\ContactInterface;
use Drupal\amoebacrm\Oauth2\RetrieveDataCrm;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Stevenmaguire\OAuth2\Client\Provider\Salesforce;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides contact POST request functionality for Salesforce integration.
 */
class SalesforceExporter extends RetrieveDataCrm {

  use StringTranslationTrait;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * The message log service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(Json $json, RequestStack $requestStack, StateInterface $state, ConfigFactoryInterface $config_factory, UrlGeneratorInterface $url_generator, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($json, $requestStack, $state);
    $this->configFactory = $config_factory;
    $this->urlGenerator = $url_generator;
    $this->messenger = $messenger;
    $this->loggerFactory = $logger_factory;
    $this->initializeProvider();
  }

  /**
   * Exports the contact to Salesforce.
   *
   * @param \Drupal\amoebacrm\Entity\ContactInterface $entity
   *   The contact entity.
   *
   * @return array|bool|ContactInterface|mixed
   *   Returns the response array, FALSE if response was unsuccessful and
   *   the contact entity if the entity already exists.
   */
  public function exportContact(ContactInterface $entity) {
    // Make sure that the entity does not have a Salesforce id.
    $salesforceId = $entity->get('salesforce_id')->getString();
    if (empty($salesforceId)) {
      // Map internal contact values to their corresponding Salesforce fields.
      $data = $this->mapDataWithCrmImporter($entity);
      if (!empty($data)) {
        $parameters = [
          'body' => $this->json->encode($data),
          'headers' => [
            'Content-Type' => 'application/json',
          ],
        ];
        $response = $this->sendDataToCrm('POST', $parameters);
        // Set the id of the contact created on Salesforce on the entity
        // field.
        if (!empty($response) && !empty($response['id'])) {
          $entity->set('salesforce_id', $response['id']);
          try {
            $entity->save();
          }
          catch (\Exception $e) {
            $this->loggerFactory->get('salesforce')->error($e->getMessage());
          }
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Map the entity field values with Salesforce parameters.
   *
   * @param \Drupal\amoebacrm\Entity\ContactInterface $entity
   *   The contact entity.
   *
   * @return array
   *   The mapped data.
   */
  protected function mapDataWithCrmImporter(ContactInterface $entity) {
    $addressValue = $entity->get('address')->getValue();
    return [
      'FirstName' => $entity->get('first_name')->getString(),
      'LastName' => $entity->get('last_name')->getString(),
      'Email' => $entity->get('email')->getString(),
      'MobilePhone' => $entity->get('mobile_phone_number')->getString(),
      'Phone' => $entity->get('phone_number')->getString(),
      'Fax' => $entity->get('fax')->getString(),
      'Salutation' => $entity->get('title')->getString(),
      'MailingStreet' => !empty($addressValue[0]['address_line1']) ? $addressValue[0]['address_line1'] : '',
      'MailingCity' => !empty($addressValue[0]['locality']) ? $addressValue[0]['locality'] : '',
      'MailingState' => !empty($addressValue[0]['state']) ? $addressValue[0]['state'] : '',
      'MailingPostalCode' => !empty($addressValue[0]['postal_code']) ? $addressValue[0]['postal_code'] : '',
      'MailingCountry' => !empty($addressValue[0]['country_code']) ? $addressValue[0]['country_code'] : '',
    ];
  }

  /**
   * Initializes and sets the provider.
   *
   * @param array $providerValues
   *   An array of provider configuration values passed when the
   *   provider configuration is not yet stored.
   */
  public function initializeProvider(array $providerValues = []) {
    $config = $this->configFactory->get('amoebacrm_importer.settings')->get('request');
    $requestConfig = isset($config['salesforce']) ? $config['salesforce'] : $providerValues;
    if (!empty($requestConfig['consumer_key']) && !empty($requestConfig['consumer_secret']) && !empty($requestConfig['domain'])) {
      $this->setGrantType('authorization_code');
      $provider = new Salesforce([
        'clientId' => $requestConfig['consumer_key'],
        'clientSecret' => $requestConfig['consumer_secret'],
        'redirectUri' => $this->urlGenerator->generateFromRoute('<current>', [], ['absolute' => TRUE]),
        'loginUrl' => $requestConfig['domain'],
      ]);
      $this->setEndpointUrl($requestConfig['domain']);
      $this->setEndpointPath('/services/data/v42.0/sobjects/Contact');
      $this->setGrantType('authorization_code');
      $this->setProvider($provider);
    }
  }

  /**
   * Makes the CRM authorization.
   *
   * @param array $config
   *   An array of provider configuration values.
   */
  public function authorizeUser(array $config) {
    $this->initializeProvider($config);
    // Authorize the client.
    $authorizationUrl = $this->provider->getAuthorizationUrl();
    // Save the state of the provider.
    $this->state->set('oauthState', $this->provider->getState());
    $redirect = new RedirectResponse($authorizationUrl);
    $redirect->send();
  }

  /**
   * Stores the access token details for later requests.
   *
   * Makes a request to get the access token and stores it using state.
   *
   * @param string $urlCode
   *   The code to be exchanged for an access token.
   * @param string $urlState
   *   The state to verify for against CSRF attacks.
   *
   * @return bool
   *   If the token is retrieved returns TRUE, FALSE otherwise.
   */
  public function storeAccessToken($urlCode = '', $urlState = '') {
    $originalState = $this->state->get('oauthState');
    if ($this->getGrantType() == 'authorization_code') {
      if ($urlState !== $originalState) {
        $this->state->delete('oauthState');
        $this->messenger->addError('State does not match!');
        return FALSE;
      }
      $this->state->delete('oauthState');
      $this->setGrantTypeOptions(['code' => $urlCode]);
    }
    $accessToken = $this->requestAccessToken();
    if (!empty($accessToken)) {
      $this->state->set('salesforce', [
        'access_token' => $accessToken->getToken(),
        'refresh_token' => $accessToken->getRefreshToken(),
        'expires' => $accessToken->getExpires(),
      ]);
      $this->loggerFactory->get('salesforce')->info('Salesforce successfully authorized!');
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Sends data to the external crm.
   *
   * @param string $method
   *   The request method.
   * @param array $parameters
   *   The request parameters.
   *
   * @return bool|mixed
   *   Returns the decoded response body if the request is successful,
   *   FALSE otherwise.
   */
  public function sendDataToCrm($method = 'POST', array $parameters = []) {
    $credentials = $this->state->get('salesforce');
    $accessToken = !empty($credentials['access_token']) ? $credentials['access_token'] : '';
    // If access token exists, try to make the request, otherwise request
    // the user to authorize.
    if (!empty($accessToken)) {
      $request = $this->provider->getAuthenticatedRequest($method, $this->getRetrieveUrl(), $accessToken, $parameters);
      if (!empty($request)) {
        try {
          $values = $this->json->decode($this->provider->getResponse($request)->getBody()->getContents());
        }
        catch (\Exception $e) {
          if ($e->getCode() === 401) {
            // If the request is not authorized and a refresh token exists, try
            // to get a new token using it.
            if (!empty($credentials['refresh_token'])) {
              $this->setGrantTypeOptions(['refresh_token' => $credentials['refresh_token']]);
              $this->setGrantType('refresh_token');
              $refreshedToken = $this->storeAccessToken();
              if ($refreshedToken) {
                $values = $this->sendDataToCrm('POST', $parameters);
              }
            }
            else {
              $errorCode = $e->getCode();
            }
          }
          else {
            $this->loggerFactory->get('salesforce')->error('Message: @message Code: @code', [
              '@message' => $e->getMessage(),
              '@code' => $e->getCode(),
            ]);
          }
        }
      }
    }
    if (empty($accessToken) || !empty($errorCode)) {
      $this->loggerFactory->get('salesforce')->error('Contacts could not be created. Please authorize your application at /admin/config/amoebacrm-salesforce/salesforce.');
    }

    return !empty($values) ? $values : FALSE;
  }

}
