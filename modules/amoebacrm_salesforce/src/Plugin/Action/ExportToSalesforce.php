<?php

namespace Drupal\amoebacrm_salesforce\Plugin\Action;

use Drupal\amoebacrm_salesforce\Services\SalesforceExporter;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates the contact in the salesforce crm.
 *
 * @Action(
 *   id = "export_to_salesforce",
 *   label = @Translation("Export contact to SalesforceCRM"),
 *   type = "actions_manager"
 * )
 */
class ExportToSalesforce extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  /**
   * The salesforce exporter service.
   *
   * @var \Drupal\amoebacrm_salesforce\Services\SalesforceExporter
   */
  protected $salesforeceExporter;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SalesforceExporter $salesforce_exporter, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->salesforeceExporter = $salesforce_exporter;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('amoebacrm_salesforce.contact_exporter'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $result = $this->salesforeceExporter->exportContact($entity);
    if ($result && $entity instanceof EntityInterface) {
      $this->loggerFactory->get('salesforce')->info('Contact with ID @ID was created in Salesforce.', ['@ID' => $entity->id()]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return AccessResultAllowed::allowed();
  }

}
