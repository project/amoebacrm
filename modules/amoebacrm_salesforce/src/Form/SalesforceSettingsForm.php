<?php

namespace Drupal\amoebacrm_salesforce\Form;

use Drupal\amoebacrm\Form\ImporterSettingsForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class SalesforceSettingsForm.
 */
class SalesforceSettingsForm extends ImporterSettingsForm {

  /**
   * The retrieve data crm service.
   *
   * @var \Drupal\amoebacrm\Oauth2\RetrieveDataCrmInterface
   */
  protected $contactExporter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->contactExporter = $container->get('amoebacrm_salesforce.contact_exporter');
    $instance->loggerFactory = $container->get('logger.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $crm_name = '') {
    $code = $this->getRequest()->get('code');
    $state = $this->getRequest()->get('state');
    if (!empty($code) && !empty($state)) {
      $authorizationStatus = $this->contactExporter->storeAccessToken($code, $state);
      if ($authorizationStatus) {
        $this->messenger()->addMessage('Successfully authorized!');
      }
      else {
        $this->messenger()->addError('There was an authorization problem. Please check the logs!');
      }
      $redirect = new RedirectResponse(Url::fromRoute('<current>')->toString());
      $redirect->send();
    }
    return parent::buildForm($form, $form_state, $crm_name);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->contactExporter->authorizeUser($form_state->getValue(['request', 'salesforce']));
  }

}
