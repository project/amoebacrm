<?php

namespace Drupal\amoebacrm_salesforce\Form;

use Drupal\amoebacrm\Form\ImporterForm;
use Drupal\amoebacrm\Traits\EntityGenerateTrait;
use Drupal\Core\Form\FormStateInterface;
use Stevenmaguire\OAuth2\Client\Provider\Salesforce;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Create authorization from for Salesforce CRM.
 */
class SalesforceImporterForm extends ImporterForm {
  use EntityGenerateTrait;

  const CRM_TYPE = 'salesforce';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'salesforce_importer';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL) {
    $form = parent::buildForm($form, $form_state, $entity_type_id);

    // Get the code from request.
    $code = $this->getRequest()->get('code');
    if (!empty($code)) {
      // Retrieve the data from the current CRM.
      $retrievedData = $this->retrieveDataCrm->getDataFromCrm();
      if (is_array($retrievedData)) {
        $this->importEntities($entity_type_id, $retrievedData);
      }
      $redirect = new RedirectResponse($this->getCurrentPageUrl());
      $redirect->send();
      exit();
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildProvider() {
    return new Salesforce([
      'clientId' => $this->requestConfig['consumer_key'],
      'clientSecret' => $this->requestConfig['consumer_secret'],
      'redirectUri' => $this->getCurrentPageUrl(),
      'loginUrl' => $this->requestConfig['domain'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareContactMappingValues(array $responseData) {
    $entities = [];
    foreach ($responseData['records'] as $contact) {
      $companyID = self::entityLookup('company', 'name', $contact['Account']['Name']);
      $countryCode = $this->getCountryCode($contact['MailingAddress']['country']);
      $address = [];
      // Don't build the address if we don't have country code.
      if (!empty($countryCode)) {
        $address = [
          'address_line1' => !empty($contact['MailingAddress']['street']) ? $contact['MailingAddress']['street'] : '',
          'locality' => !empty($contact['MailingAddress']['city']) ? $contact['MailingAddress']['city'] : '',
          'state' => !empty($contact['MailingAddress']['state']) ? $contact['MailingAddress']['state'] : '',
          'zip_code' => !empty($contact['MailingAddress']['postalCode']) ? $contact['MailingAddress']['postalCode'] : '',
          'country_code' => !empty($countryCode) ? $countryCode : '',
        ];
      }
      $entities[] = [
        'first_name' => !empty($contact['FirstName']) ? $contact['FirstName'] : '',
        'last_name' => isset($contact['LastName']) ? $contact['LastName'] : '',
        'company' => !empty($companyID) ? $companyID : '',
        'email' => isset($contact['Email']) ? $contact['Email'] : '',
        'mobile_phone_number' => isset($contact['MobilePhone']) ? $contact['MobilePhone'] : '',
        'phone_number' => isset($contact['Phone']) ? $contact['Phone'] : '',
        'fax' => isset($contact['Fax']) ? $contact['Fax'] : '',
        'title' => isset($contact['Salutation']) ? $contact['Salutation'] : '',
        'address' => !empty($address) ? $this->processAddressData($address) : '',
        'salesforce_id' => !empty($contact['Id']) ? $contact['Id'] : '',
      ];
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareCompanyMappingValues(array $responseData) {
    $entities = [];
    foreach ($responseData['records'] as $company) {
      $industry = self::taxonomyGenerate($company['Industry'], 'industries');
      $countryCode = $this->getCountryCode($company['BillingAddress']['country']);
      $address = [];
      if (!empty($countryCode)) {
        $address = [
          'address_line1' => !empty($company['BillingAddress']['street']) ? $company['BillingAddress']['street'] : '',
          'locality' => !empty($company['BillingAddress']['city']) ? $company['BillingAddress']['city'] : '',
          'state' => !empty($company['BillingAddress']['state']) ? $company['BillingAddress']['state'] : '',
          'zip_code' => !empty($company['BillingAddress']['postalCode']) ? $company['BillingAddress']['postalCode'] : '',
          'country_code' => !empty($countryCode) ? $countryCode : '',
        ];
      }
      $entities[] = [
        'name' => !empty($company['Name']) ? $company['Name'] : '',
        'website' => $this->urlIsValid($company['Website']),
        'number_of_employees' => !empty($company['NumberOfEmployees']) ? $company['NumberOfEmployees'] : '',
        'mobile_phone_number' => !empty($company['MobilePhone']) ? $company['MobilePhone'] : '',
        'phone_number' => !empty($company['Phone']) ? $company['Phone'] : '',
        'fax' => !empty($company['Fax']) ? $company['Fax'] : '',
        'industry' => !empty($industry) ? $industry : '',
        'description' => !empty($company['Description']) ? $company['Description'] : '',
        'address' => !empty($address) ? $this->processAddressData($address) : '',
      ];
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEndpointPath($entityType = '') {
    $selectedFields = '';
    $fieldsRetrieve = [
      'company' => [
        'Name',
        'Website',
        'Phone',
        'NumberOfEmployees',
        'Fax',
        'Industry',
        'Description',
        'BillingAddress',
      ],
      'contact' => [
        'Contact.Account.Name',
        'FirstName',
        'LastName',
        'Email',
        'MobilePhone',
        'Phone',
        'Fax',
        'Salutation',
        'MailingAddress',
        'Id',
      ],
    ];
    if ($entityType === 'company') {
      $selectedFields = 'SELECT+' . implode(',', $fieldsRetrieve[$entityType]) . '+from+Account';
    }
    elseif ($entityType === 'contact') {
      $selectedFields = 'SELECT+' . implode(',', $fieldsRetrieve[$entityType]) . '+from+' . ucfirst($entityType);
    }
    $url = 'https://eu16.salesforce.com/services/data/v42.0/query/?q=';

    return $url . $selectedFields;
  }

}
