<?php

namespace Drupal\amoebacrm_rest_api\Plugin\rest\resource;

use Drupal\amoebacrm\Entity\ContactInterface;
use Drupal\amoebacrm\Traits\EntityGenerateTrait;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\Plugin\rest\resource\EntityResourceValidationTrait;
use Drupal\rest\ResourceResponse;

/**
 * Rest plugin to create a contact entity.
 *
 * @RestResource(
 *   id = "entity:contact",
 *   label = @Translation("AmoebaCRM - Create a contact entity"),
 *   uri_paths = {
 *     "create" = "/api/contact/create"
 *   }
 * )
 */
class CreateContact extends ResourceBase {

  use EntityResourceValidationTrait;
  use EntityGenerateTrait;

  /**
   * {@inheritdoc}
   */
  public function post(ContactInterface $entity) {
    $companyField = $entity->get('company')->target_id;
    // Lookup for existing companies and set it on the field.
    $companyExistingValue = $this->entityLookup('company', 'name', $companyField);
    if (!empty($companyExistingValue)) {
      $entity->set('company', $companyExistingValue);
    }
    $responseArray['message'] = $this->t('The contact entity was created!');
    $definition = $this->getPluginDefinition();

    // Verify that the deserialized entity is of the type that we expect to
    // prevent security issues.
    if ($entity->getEntityTypeId() != $definition['entity_type']) {
      $responseArray['error'] = $this->t('Invalid entity type');
    }
    // POSTed entities must not have an ID set, because we always want to create
    // new entities here.
    if (!$entity->isNew()) {
      $responseArray['error'] = $this->t('Only new entities can be created');
    }

    // Validate the received data before saving.
    $this->validate($entity);
    try {
      $entity->save();
      $this->logger->notice($this->t('Created entity %type with ID %id.'), ['%type' => $entity->getEntityTypeId(), '%id' => $entity->id()]);
      $responseArray['id'] = $entity->id();
    }
    catch (EntityStorageException $e) {
      $responseArray['error'] = $this->t('Internal Server Error');
    }

    return new ResourceResponse($responseArray);
  }

}
