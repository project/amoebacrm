<?php

namespace Drupal\amoebacrm_rest_api\Plugin\rest\resource;

use Drupal\amoebacrm\Entity\CompanyInterface;
use Drupal\amoebacrm\Traits\EntityGenerateTrait;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\Plugin\rest\resource\EntityResourceValidationTrait;
use Drupal\rest\ResourceResponse;

/**
 * Rest plugin to create a company entity.
 *
 * @RestResource(
 *   id = "entity:company",
 *   label = @Translation("AmoebaCRM - Create a company entity"),
 *   uri_paths = {
 *     "create" = "/api/company/create"
 *   }
 * )
 */
class CreateCompany extends ResourceBase {
  use EntityGenerateTrait;
  use EntityResourceValidationTrait;

  /**
   * {@inheritdoc}
   */
  public function post(CompanyInterface $entity) {
    $industryValue = $entity->get('industry')->value;
    // Lookup for existing industries terms and set it on the field.
    $industryExistingValue = $this->entityLookup('taxonomy_term', 'name', $industryValue);
    if (!empty($industryExistingValue)) {
      $entity->set('industry', $industryExistingValue);
    }
    else {
      // Create the industry with the name given in the post request.
      $newlyCreatedId = $this->taxonomyGenerate($industryValue, 'industries');
      // Lookup the industry newly created and set it on the field.
      $entity->set('industry', $newlyCreatedId);
    }
    $definition = $this->getPluginDefinition();
    $response['message'] = $this->t('The company entity was successfully created');
    // Verify that the deserialized entity is of the type that we expect to
    // prevent security issues.
    if ($entity->getEntityTypeId() != $definition['entity_type']) {
      $response['error'] = $this->t('Invalid entity type');
    }
    // Posted entities must not have an ID set, because we always want to create
    // new entities here.
    if (!$entity->isNew()) {
      $response['error'] = $this->t('Only new entities can be created');
    }

    // Validate the received data before saving.
    $this->validate($entity);
    try {
      $entity->save();
      $this->logger->notice($this->t('Created entity %type with ID %id.'), ['%type' => $entity->getEntityTypeId(), '%id' => $entity->id()]);
      $response['id'] = $entity->id();
    }
    catch (EntityStorageException $e) {
      $response = $this->t('Internal Server Error');
    }
    return new ResourceResponse($response);
  }

}
