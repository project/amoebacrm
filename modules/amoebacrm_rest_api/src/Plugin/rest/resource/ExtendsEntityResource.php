<?php

namespace Drupal\amoebacrm_rest_api\Plugin\rest\resource;

use Drupal\amoebacrm\Traits\EntityGenerateTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;

/**
 * Rest plugin to update existing entities with the new data.
 *
 * @RestResource(
 *   id = "extends_entity_resource",
 *   label = @Translation("Extends Entity Resource"),
 *   serialization_class = "Drupal\Core\Entity\Entity",
 *   deriver = "Drupal\rest\Plugin\Deriver\EntityDeriver",
 *   uri_paths = {
 *     "canonical" = "/api/resource/{entity_type}"
 *   }
 * )
 */
class ExtendsEntityResource extends EntityResource {

  use EntityGenerateTrait;

  /**
   * {@inheritdoc}
   */
  public function patch(EntityInterface $original_entity, EntityInterface $entity = NULL) {
    // Check if field company exists.
    if ($entity->hasField('company')) {
      // Get the company name.
      $companyField = $entity->get('company')->target_id;
      if (!empty($companyField)) {
        // Lookup for existing companies and set it on the field.
        $companyExistingValue = $this->entityLookup('company', 'name', $companyField);
        // Set the value on the field.
        if (!empty($companyExistingValue)) {
          $entity->set('company', $companyExistingValue);
        }
      }
    }
    // Check if industry exists and get the industry name.
    if ($entity->hasField('industry')) {
      $industryValue = $entity->get('industry')->value;
      if (!empty($industryValue)) {
        // Lookup for existing industries terms and set it on the field.
        $industryExistingValue = $this->entityLookup('taxonomy_term', 'name', $industryValue);
        if (!empty($industryExistingValue)) {
          $entity->set('industry', $industryExistingValue);
        }
        else {
          // Create the industry with the name given in the post request.
          $newlyCreatedId = $this->taxonomyGenerate($industryValue, 'industries');
          // Lookup the industry newly created and set it on the field.
          $entity->set('industry', $newlyCreatedId);
        }
      }
    }
    return parent::patch($original_entity, $entity);
  }

}
