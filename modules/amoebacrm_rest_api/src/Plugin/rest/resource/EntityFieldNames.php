<?php

namespace Drupal\amoebacrm_rest_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Rest plugin to retrieve available entity fields.
 *
 * @RestResource(
 *   id = "retrieve_entity_fields",
 *   label = @Translation("AmoebaCRM - Retrieve entity fields"),
 *   uri_paths = {
 *     "canonical" = "/api/{entity_type}/fields"
 *   }
 * )
 */
class EntityFieldNames extends ResourceBase {

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, $logger, EntityFieldManagerInterface $entityFieldManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function get($entity_type) {
    if (!empty($entity_type)) {
      // Prepare the fields which has to be unset from the retrieving data.
      $fieldsToUnset = [
        'id', 'uuid', 'vid', 'langcode', 'user_id',
        'created', 'changed', 'type', 'status', 'default_langcode',
        'revision_default', 'revision_translation_affected',
        'moderation_state', 'address', 'langcode', 'dependent_locality',
        'sorting_code', 'organization', 'given_name', 'additional_name', 'family_name',
      ];

      // Initialize the arrays which will contain the retrieved fields.
      $finalAddressFields = [];
      $finalEntityFields = [];
      // Get all the entity fields.
      $entityFields = $this->entityFieldManager->getFieldDefinitions($entity_type, '');
      // Check if there are fields on the entity.
      if (is_array($entityFields)) {
        foreach ($entityFields as $field) {
          if (!in_array($field->getName(), $fieldsToUnset)) {
            $finalEntityFields[$field->getName()] = $field->getLabel()->render();
          }
        }
      }
      // Check if the entity contains address fields.
      if (isset($entityFields['address'])) {
        // Get all the address fields and unset the unnecessary ones.
        $addressFields = $entityFields['address']->getPropertyDefinitions();
        foreach ($addressFields as $key => $field) {
          if (!in_array($key, $fieldsToUnset)) {
            $finalAddressFields[$key] = $field->getLabel()->render();
          }
        }
      }
      // Get all the arrays into one.
      $response['fields'] = array_merge($finalAddressFields, $finalEntityFields);
    }
    else {
      $response['message'] = $this->t('No entities found.');
    }

    return new ResourceResponse($response);
  }

}
