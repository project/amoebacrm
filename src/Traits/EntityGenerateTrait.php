<?php

namespace Drupal\amoebacrm\Traits;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;

/**
 * Trait EntityGenerateTrait.
 *
 * @package Drupal\amoebacrm\Traits
 */
trait EntityGenerateTrait {

  /**
   * Generates a taxonomy term if it doesn't exist.
   *
   * @param string $value
   *   The name of the taxonomy term.
   * @param string $taxonomy_name
   *   The name of the vocabulary.
   *
   * @return array|int|null|string
   *
   *   If the term already exists or it's successfully created then
   *   it returns its id, otherwise an empty string.
   */
  protected static function taxonomyGenerate($value, $taxonomy_name) {
    if (!empty($value)) {
      $entity_type_manager = \Drupal::entityTypeManager();
      $result = self::entityLookup('taxonomy_term', 'name', $value);
      if (empty($result)) {
        try {
          $entity = $entity_type_manager->getStorage('taxonomy_term')->create(['name' => $value, 'vid' => $taxonomy_name]);
          $entity->save();
          return $entity->id();
        }
        catch (EntityStorageException $e) {
          watchdog_exception('amoebacrm', $e);
        }
        catch (InvalidPluginDefinitionException $e) {
          watchdog_exception('amoebacrm', $e);
        }
        catch (PluginNotFoundException $e) {
          watchdog_exception('amoebacrm', $e);
        }
      }
    }
    return !empty($result) ? key($result) : FALSE;
  }

  /**
   * Checks if an entity of a given type exists.
   *
   * @param string $entityType
   *   The type of the entity to be checked.
   * @param string $fieldName
   *   The entity field to check against.
   * @param string $value
   *   The value to be compared.
   *
   * @return array|int|string
   *   If the entity exists it returns its id else it returns an empty string.
   */
  protected static function entityLookup($entityType, $fieldName, $value) {
    $entity_type_manager = \Drupal::entityTypeManager();
    try {
      $query = $entity_type_manager->getStorage($entityType)->getQuery()->condition($fieldName, $value, '=');
      $result = $query->execute();
    }
    catch (InvalidPluginDefinitionException $e) {
      watchdog_exception('amoebacrm', $e);
    }
    catch (PluginNotFoundException $e) {
      watchdog_exception('amoebacrm', $e);
    }

    return !empty($result) ? $result : FALSE;
  }

  /**
   * Creates a new entity.
   *
   * @param string $entityType
   *   The type of the entity to be checked.
   * @param array $entity
   *   The entity object to be saved.
   *
   * @return bool
   *   Return True if the entity was save, FALSE otherwise.
   */
  protected static function entityCreate($entityType, array $entity) {
    $entity_type_manager = \Drupal::entityTypeManager();
    try {
      if ($entity_type_manager->getStorage($entityType)->create($entity)->save()) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
    catch (InvalidPluginDefinitionException $e) {
      watchdog_exception('amoebacrm', $e);
    }
    catch (PluginNotFoundException $e) {
      watchdog_exception('amoebacrm', $e);
    }
    catch (EntityStorageException $e) {
      watchdog_exception('amoebacrm', $e);
    }
  }

  /**
   * If the force update checkbox is checked it updates the entity.
   *
   * @param string $entityType
   *   The type of the entity to be updated.
   * @param int $entityID
   *   The ID of the entity to be updated.
   * @param array $newEntityFields
   *   An array of key, value type containing the fields to be updated.
   *
   * @return int|bool
   *   Integer if the entity was saved, FALSE otherwise.
   */
  protected static function updateEntity($entityType, $entityID, array $newEntityFields) {
    try {
      // Gets the entity by type and id.
      $query = \Drupal::entityTypeManager()->getStorage($entityType)->loadByProperties(['id' => $entityID]);
      if (is_array($query)) {
        // Expect only one result, get rid of first level of array.
        $entity = reset($query);
        // Check if the entity is correctly loaded.
        if ($entity instanceof EntityInterface) {
          // Update all the fields with the new values.
          foreach ($newEntityFields as $machineName => $value) {
            if (!empty($machineName) && !empty($value)) {
              $entity->set($machineName, $value);
            }
          }
          return $entity->save();
        }
      }
    }
    catch (InvalidPluginDefinitionException $e) {
      watchdog_exception('amoebacrm', $e);
    }
    catch (EntityStorageException $e) {
      watchdog_exception('amoebacrm', $e);
    }
    catch (PluginNotFoundException $e) {
      watchdog_exception('amoebacrm', $e);
    }
    return FALSE;
  }

}
