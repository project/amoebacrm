<?php

namespace Drupal\amoebacrm;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Contact entities.
 *
 * @ingroup contact
 */
class ContactListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Contact ID');
    $header['name'] = $this->t('Name');
    $header['title'] = $this->t('Title');
    $header['company'] = $this->t('The company referred');
    $header['email'] = $this->t('Email');
    $header['address'] = $this->t('Address');
    $header['phone_number'] = $this->t('Phone');
    $header['mobile_phone_number'] = $this->t('Mobile Phone');
    $header['fax'] = $this->t('Fax');
    $header['website'] = $this->t('Website');
    $header['facebook'] = $this->t('Facebook');
    $header['instagram'] = $this->t('Instagram');
    $header['linkedin'] = $this->t('LinkedIn');
    $header['twitter'] = $this->t('Twitter');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\amoebacrm\Entity\Contact */
    $url = Url::fromRoute('entity.contact.canonical', ['contact' => $entity->id()]);
    $row['id'] = $entity->id();
    $row['name'] = new Link($entity->getName(), $url);
    $row['title'] = $entity->get('title')->value;
    $row['company'] = $entity->getCompanyName();
    $row['email'] = $entity->get('email')->value;
    $row['address'] = !empty($entity->prepareAddressToDisplay()) ? implode(', ', $entity->prepareAddressToDisplay()) : '';
    $row['phone_number'] = $entity->get('phone_number')->value;
    $row['mobile_phone_number'] = $entity->get('mobile_phone_number')->value;
    $row['fax'] = $entity->get('fax')->value;
    $row['website'] = $entity->get('website')->value;
    $row['facebook'] = $entity->get('facebook')->value;
    $row['instagram'] = $entity->get('instagram')->value;
    $row['linkedin'] = $entity->get('linkedin')->value;
    $row['twitter'] = $entity->get('twitter')->value;

    return $row + parent::buildRow($entity);
  }

}
