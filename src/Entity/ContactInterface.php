<?php

namespace Drupal\amoebacrm\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Contact entities.
 *
 * @ingroup contact
 */
interface ContactInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the Contact name.
   *
   * @return string
   *   First name of the Contact.
   */
  public function getName();

  /**
   * Sets the Contact name.
   *
   * @param string $name
   *   The full name of the Contact.
   *
   * @return \Drupal\amoebacrm\Entity\ContactInterface
   *   The called Contact entity.
   */
  public function setName($name);

  /**
   * Gets the last name of the Contact.
   *
   * @return string
   *   Last name of the Contact.
   */
  public function getLastName();

  /**
   * Sets the Contact last name.
   *
   * @param string $last_name
   *   Last name of the contact.
   *
   * @return \Drupal\amoebacrm\Entity\ContactInterface
   *   The called Contact entity.
   */
  public function setLastName($last_name);

  /**
   * Gets the first name of the Contact.
   *
   * @return string
   *   Last name of the Contact.
   */
  public function getTitle();

  /**
   * Sets the Contact title.
   *
   * @param string $title
   *   Last title of the contact.
   *
   * @return \Drupal\amoebacrm\Entity\ContactInterface
   *   The called Contact entity.
   */
  public function setTitle($title);

  /**
   * Gets the first name of the Contact.
   *
   * @return string
   *   Last name of the Contact.
   */
  public function getFirstName();

  /**
   * Sets the Contact first name.
   *
   * @param string $first_name
   *   Last name of the contact.
   *
   * @return \Drupal\amoebacrm\Entity\ContactInterface
   *   The called Contact entity.
   */
  public function setFirstName($first_name);

  /**
   * Gets the Contact creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Contact.
   */
  public function getCreatedTime();

  /**
   * Sets the Contact creation timestamp.
   *
   * @param int $timestamp
   *   The Contact creation timestamp.
   *
   * @return \Drupal\amoebacrm\Entity\ContactInterface
   *   The called Contact entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Contact company name.
   *
   * @return string
   *   The company name of the Contact.
   */
  public function getCompanyName();

}
