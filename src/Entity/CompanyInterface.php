<?php

namespace Drupal\amoebacrm\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Company entities.
 *
 * @ingroup company
 */
interface CompanyInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the Company name.
   *
   * @return string
   *   Name of the Company.
   */
  public function getName();

  /**
   * Sets the Company name.
   *
   * @param string $name
   *   The Company name.
   *
   * @return \Drupal\amoebacrm\Entity\CompanyInterface
   *   The called Company entity.
   */
  public function setName($name);

  /**
   * Gets the Company email.
   *
   * @return string
   *   Email of the Company.
   */
  public function getEmail();

  /**
   * Sets the Company email.
   *
   * @param string $email
   *   The Company email.
   *
   * @return \Drupal\amoebacrm\Entity\CompanyInterface
   *   The called Company entity.
   */
  public function setEmail($email);

  /**
   * Gets the Company creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Company.
   */
  public function getCreatedTime();

  /**
   * Sets the Company creation timestamp.
   *
   * @param int $timestamp
   *   The Company creation timestamp.
   *
   * @return \Drupal\amoebacrm\Entity\CompanyInterface
   *   The called Company entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Company industry name referenced entity.
   *
   * @return string|bool
   *   The name of industry term if exists, FALSE otherwise.
   */
  public function getIndustryName();

}
