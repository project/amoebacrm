<?php

namespace Drupal\amoebacrm\Entity;

use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Exception\UnsupportedEntityTypeDefinitionException;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines the Contact entity.
 *
 * @ingroup contact
 *
 * @ContentEntityType(
 *   id = "contact",
 *   label = @Translation("Contact"),
 *   label_collection = @Translation("Contacts"),
 *   label_singular = @Translation("contact"),
 *   label_plural = @Translation("contacts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count contact",
 *     plural = "@count contacts"
 *   ),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\amoebacrm\ContactListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\amoebacrm\Form\ContactForm",
 *       "edit" = "Drupal\amoebacrm\Form\ContactForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\amoebacrm\ContactAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\amoebacrm\ContactHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "contact",
 *   data_table = "contact_field_data",
 *   revision_table = "contact_revision",
 *   revision_data_table = "contact_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer contact entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "revision" = "vid",
 *     "type" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *     "published" = "status",
 *     "created" = "created",
 *   },
 *   links = {
 *     "canonical" = "/contact/{contact}",
 *     "add-form" = "/contact/add",
 *     "edit-form" = "/contact/{contact}/edit",
 *     "delete-form" = "/contact/{contact}/delete",
 *     "collection" = "/admin/content/amoebacrm/contacts",
 *   },
 *   field_ui_base_route = "contact.settings"
 * )
 */
class Contact extends ContentEntityBase implements ContactInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    // Set automatically the type on the entity.
    $this->setType($this->entityTypeId);
    // Automatically update the label of the entity based on the first name and
    // last name fields.
    $first_name = $this->getFirstName();
    $first_name = !empty($first_name[0]['value']) ? $first_name[0]['value'] : '';
    $last_name = $this->getLastName();
    $last_name = !empty($last_name[0]['value']) ? $last_name[0]['value'] : '';
    $full_name = trim("$first_name $last_name");
    if (empty($full_name)) {
      $full_name = 'John Doe';
    }
    $this->set('name', $full_name);

  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($reverse = FALSE) {
    $first_name = $this->get('first_name');
    $last_name = $this->get('last_name');
    $full_name = $reverse ? "$last_name $first_name" : "$first_name $last_name";
    return $this->set('name', $full_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstName() {
    return $this->get('first_name')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setFirstName($firstName) {
    return $this->set('first_name', $firstName);
  }

  /**
   * {@inheritdoc}
   */
  public function getLastName() {
    return $this->get('last_name')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function setLastName($lastName) {
    return $this->set('last_name', $lastName);
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    return $this->set('title', $title);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published = NULL) {
    $this->set('status', TRUE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUnpublished() {
    $this->set('status', FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setType($type) {
    $this->set('type', $type);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->get('type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareAddressToDisplay() {
    $address = $this->get('address')->getValue();
    $final_address = [];
    if (isset($address[0])) {
      foreach ($address[0] as $label => $field) {
        if (!empty($field)) {
          $final_address[$label] = $field;
        }
      }
    }
    return !empty($final_address) ? $final_address : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCompanyName() {
    $company = $this->get('company');
    return !empty($company->target_id) ? $company->entity->getName() : '';
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Contact entity.'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 100,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Contact entity.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the Contact entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the Contact entity was last edited.'));

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('The type of the entity.'))
      ->setDefaultValue('');

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('Mr/Mrs'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['first_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('First Name'))
      ->setDescription(t('The name of the Contact.'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['last_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Last Name'))
      ->setDescription(t('The last name of the Contact.'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['email'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Email'))
      ->setDescription(t('The email of the Contact.'))
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'email_default',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['company'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('The contact company'))
      ->setDescription(t('The contact company that is referred to.'))
      ->setSetting('target_type', 'company')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_complex',
        'settings' => [
          'form_mode' => 'default',
          'collapsible' => TRUE,
          'allow_new' => TRUE,
          'allow_existing' => TRUE,
          'match_operator' => 'CONTAINS',
          'override_labels' => FALSE,
          'collapsed' => FALSE,
          'allow_duplicate' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['address'] = BaseFieldDefinition::create('address')
      ->setLabel(t('Address'))
      ->setDescription(t('The address of the Contact.'))
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'address_default',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setSettings([
        'field_overrides' => [
          'givenName' => ['override' => 'hidden'],
          'additionalName' => ['override' => 'hidden'],
          'familyName' => ['override' => 'hidden'],
          'organization' => ['override' => 'hidden'],
          'addressLine1' => ['override' => 'optional'],
          'addressLine2' => ['override' => 'optional'],
          'locality' => ['override' => 'optional'],
          'dependentLocality' => ['override' => 'hidden'],
          'administrativeArea' => ['override' => 'optional'],
          'postalCode' => ['override' => 'optional'],
          'sortingCode' => ['override' => 'hidden'],
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['mobile_phone_number'] = BaseFieldDefinition::create('telephone')
      ->setLabel(t('Mobile'))
      ->setDescription(t('The mobile number of the Contact.'))
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'telephone_default',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['phone_number'] = BaseFieldDefinition::create('telephone')
      ->setLabel(t('Phone'))
      ->setDescription(t('The phone number of the Contact.'))
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'telephone_default',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['fax'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Fax'))
      ->setDescription(t('The fax of the Contact.'))
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['website'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Website'))
      ->setDescription(t('The website of the Contact.'))
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'link',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['facebook'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Facebook'))
      ->setDescription(t('The facebook profile link of the Contact.'))
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'link',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['instagram'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Instagram'))
      ->setDescription(t('The instagram profile link of the Contact.'))
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'link',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['linkedin'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('LinkedIn'))
      ->setDescription(t('The LinkedIn profile link of the Contact.'))
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'link',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['twitter'] = BaseFieldDefinition::create('uri')
      ->setLabel(t('Twitter'))
      ->setDescription(t('The twitter profile link of the Contact.'))
      ->setDefaultValue('')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'link',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Add the published field.
    try {
      $fields += static::publishedBaseFieldDefinitions($entity_type);
    }
    catch (UnsupportedEntityTypeDefinitionException $e) {
      watchdog_exception('amoebacrm', $e);
    }

    return $fields;
  }

}
