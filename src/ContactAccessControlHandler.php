<?php

namespace Drupal\amoebacrm;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Contact entity.
 *
 * @see \Drupal\amoebacrm\Entity\Contact.
 */
class ContactAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\amoebacrm\Entity\ContactInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermissions($account, ['view unpublished contact entities', 'administer contact content'], 'OR');
        }
        return AccessResult::allowedIfHasPermissions($account, ['view published contact entities', 'administer contact content'], 'OR');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit contact entities', 'administer contact content'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete contact entities', 'administer contact content'], 'OR');
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['add contact entities', 'administer contact content'], 'OR');
  }

}
