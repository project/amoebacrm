<?php

namespace Drupal\amoebacrm\Form;

/**
 * Creates import form for CRM.
 */
interface ImporterFormInterface {

  /**
   * Returns the current page url.
   *
   * @return string
   *   Returns the current page url.
   */
  public function getCurrentPageUrl();

  /**
   * Imports the entities using the request response data.
   *
   * @param string $entityType
   *   The type of the entity to import.
   * @param array $responseData
   *   The data received from the retrieve request.
   */
  public function importEntities($entityType, array $responseData);

  /**
   * Prepares the Contact mapping values.
   *
   * @param array $responseData
   *   The data received from the retrieve request.
   */
  public function prepareContactMappingValues(array $responseData);

  /**
   * Prepares the Company mapping values.
   *
   * @param array $responseData
   *   The data received from the retrieve request.
   */
  public function prepareCompanyMappingValues(array $responseData);

  /**
   * Set the address field values.
   *
   * @param array $addressData
   *   The address data received from the retrieve request.
   */
  public function processAddressData(array $addressData);

}
