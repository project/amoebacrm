<?php

namespace Drupal\amoebacrm\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\workflows\Form\WorkflowTransitionEditForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AmoebacrmWorkflowTransitionEditForm.
 */
class AmoebacrmWorkflowTransitionEditForm extends WorkflowTransitionEditForm {

  /**
   * The action plugin manager.
   *
   * @var \Drupal\Core\Action\ActionManager
   */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->manager = $container->get('plugin.manager.action');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $workflow_transition = NULL) {
    $form = parent::buildForm($form, $form_state, $workflow_transition);
    $actionsConfiguration = $this->entityTypeManager->getStorage('action')->loadByProperties(['type' => 'actions_manager']);
    $existingActions = [];
    foreach ($actionsConfiguration as $id => $actionConfiguration) {
      $existingActions[$id] = $actionConfiguration->label();
    }
    $workflow = $this->getEntity();
    $workflowTypeSettings = $workflow->get('type_settings');
    $defaultOptions = [];
    if (isset($workflowTypeSettings['transitions'][$this->transitionId]['actions'])) {
      $defaultOptions = $workflowTypeSettings['transitions'][$this->transitionId]['actions'];
    }
    if (!empty($existingActions)) {
      $form['configured_actions'] = [
        '#type' => 'details',
        '#title' => $this->t('Choose to trigger an existing action'),
        '#attributes' => ['class' => ['container-inline']],
        '#open' => TRUE,
      ];
      $form['configured_actions']['actions'] = [
        '#type' => 'select',
        '#title' => $this->t('Select an action'),
        '#options' => $existingActions,
        '#default_value' => $defaultOptions,
        '#multiple' => TRUE,
      ];
    }
    $actions = [];
    foreach ($this->manager->getDefinitionsByType('actions_manager') as $id => $definition) {
      if (is_subclass_of($definition['class'], '\Drupal\Core\Plugin\PluginFormInterface')) {
        $actions[$id] = $definition['label'];
      }
    }

    asort($actions);
    if (!empty($actions)) {
      $form['parent'] = [
        '#type' => 'details',
        '#title' => $this->t('Create an advanced action'),
        '#attributes' => ['class' => ['container-inline']],
        '#open' => TRUE,
      ];
      $form['parent']['action'] = [
        '#type' => 'select',
        '#title' => $this->t('Action'),
        '#title_display' => 'invisible',
        '#options' => $actions,
        '#empty_option' => $this->t('- Select -'),
      ];
      $form['parent']['actions'] = [
        '#type' => 'actions',
      ];
      $form['parent']['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Create'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    if ($form_state->getValue('actions')) {
      $actions = [];
      /* @var \Drupal\workflows\WorkflowInterface $workflow */
      $workflow = $this->getEntity();
      $workflowTypeSettings = $workflow->get('type_settings');
      if (isset($workflowTypeSettings['transitions'][$this->transitionId]['actions'])) {
        if (is_array($form_state->getValue('actions'))) {
          foreach ($form_state->getValue('actions') as $action) {
            if (!in_array($action, $workflowTypeSettings['transitions'][$this->transitionId]['actions'])) {
              // Add the actions defined on the type settings
              // config entity section (transition).
              $actions[] = $action;
            }
          }
        }
      }
      // Override the default workflow transitions actions with the new ones.
      $workflowTypeSettings['transitions'][$this->transitionId]['actions'] = $actions;
      $workflow->set('type_settings', $workflowTypeSettings);
    }
    $current_path = \Drupal::service('path.current')->getPath();
    // Treat the new actions case.
    if ($form_state->getValue('action')) {
      $form_state->setRedirect(
          'action.admin_add',
          ['action_id' => $form_state->getValue('action')],
          ['query' => ['destination' => $current_path]]
      );
    }
  }

}
