<?php

namespace Drupal\amoebacrm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * General settings form for entity importer configuration.
 */
class ImporterSettingsForm extends ConfigFormBase {

  /**
   * The config settings name.
   *
   * @var string Config settings
   */
  const SETTINGS = 'amoebacrm_importer.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [static::SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amoebacrm_importer_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $crm_name = '') {
    $config = $this->config(static::SETTINGS)->get('request');

    $form['request'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Importer credentials'),
      '#tree' => TRUE,
    ];

    $form['crm_name'] = [
      '#type' => 'value',
      '#value' => $crm_name,
    ];

    $form['request'][$crm_name]['consumer_key'] = [
      '#title' => $this->t('Consumer key'),
      '#type' => 'textfield',
      '#description' => $this->t('Consumer key of your CRM application you want to grant access to.'),
      '#required' => TRUE,
      '#default_value' => isset($config[$crm_name]['consumer_key']) ? $config[$crm_name]['consumer_key'] : '',
    ];

    $form['request'][$crm_name]['consumer_secret'] = [
      '#title' => $this->t('Consumer secret'),
      '#type' => 'password',
      '#description' => $this->t('Consumer secret of your CRM application you want to grant access to.'),
      '#required' => TRUE,
      '#default_value' => isset($config[$crm_name]['consumer_secret']) ? $config[$crm_name]['consumer_secret'] : '',
    ];

    $form['request'][$crm_name]['domain'] = [
      '#title' => $this->t('Crm domain URL'),
      '#type' => 'textfield',
      '#description' => $this->t('Enter the domain of your CRM application (without trailing slash).'),
      '#default_value' => isset($config[$crm_name]['domain']) ? $config[$crm_name]['domain'] : '',
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $crm_name = $form_state->getValue('crm_name');
    $this->config(static::SETTINGS)
      ->set("request.$crm_name", $form_state->getValue(['request', $crm_name]))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
