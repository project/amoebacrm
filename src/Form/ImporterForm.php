<?php

namespace Drupal\amoebacrm\Form;

use Drupal\amoebacrm\ImporterCrmBatchExecutable;
use Drupal\amoebacrm\Oauth2\RetrieveDataCrmInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManager;
use Drupal\Core\State\State;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates general importer form for Amoeba CRMs.
 */
abstract class ImporterForm extends FormBase implements ImporterFormInterface {

  /**
   * The config settings name.
   */
  const SETTINGS = 'amoebacrm_importer.settings';

  /**
   * The config settings name particularity.
   */
  const CRM_TYPE = 'default';

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The retrieve data crm object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $retrieveDataCrm;

  /**
   * An array with the current configs.
   *
   * @var array
   */
  protected $requestConfig;

  /**
   * Entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Drupal state storage.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Mautic CRM Importer constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\amoebacrm\Oauth2\RetrieveDataCrmInterface $retrieveDataCrm
   *   The Amoeba retrieve data crm object.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entityManager
   *   The entity manager service.
   * @param \Drupal\Core\State\State $state
   *   Drupal state storage.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RetrieveDataCrmInterface $retrieveDataCrm, EntityManagerInterface $entityManager, State $state) {
    $this->configFactory = $config_factory;
    $this->retrieveDataCrm = $retrieveDataCrm;
    $this->entityManager = $entityManager;
    $this->state = $state;
    // Set the CRM type.
    $this->retrieveDataCrm->setCrmType(static::CRM_TYPE);
    // Get the configuration for the current form type.
    $config = $this->configFactory->get(static::SETTINGS)->get('request');
    $this->requestConfig = isset($config[static::CRM_TYPE]) ? $config[static::CRM_TYPE] : [];
    // Initialize the CRM data retrieve information.
    $this->initializeRetrieveDataCrm();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('amoebacrm.data_retrieve'),
      $container->get('entity.manager'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL) {
    $form['import'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];
    $form['force_update'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force Update'),
      '#description' => $this->t("If you check this and companies with the same
       name or contacts with same email are found it will update their details 
       otherwise it will not import them."),
    ];
    $form_state->set('entity_type_id', $entity_type_id);

    // Set general defaults for the retrieve data crm object.
    $endpointPath = $this->buildEndpointPath($entity_type_id);
    $this->retrieveDataCrm->setEndpointPath($endpointPath);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($this->requestConfig) || empty($this->requestConfig['domain'])) {
      $form_state->setErrorByName('import', $this->t('You need to first configure the Crm credentials in order to use the importer.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->state->set('force_update', $form_state->getValue('force_update'));
    // Retrieve the data from the current CRM.
    $retrievedData = $this->retrieveDataCrm->getDataFromCrm();
    // Request a new access token if the previous request return expired
    // from an 401 Unauthorize request.
    if ($retrievedData == 'expired') {
      $retrievedData = $this->retrieveDataCrm->getDataFromCrm();
    }
    if (is_array($retrievedData)) {
      $this->importEntities($form_state->get('entity_type_id'), $retrievedData);
    }
    else {
      $this->messenger()->addError('Something went wrong with the importer. Please check the logs for more information.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentPageUrl() {
    return $this->getUrlGenerator()->generateFromRoute('<current>', [], ['absolute' => TRUE]);
  }

  /**
   * {@inheritdoc}
   */
  public function importEntities($entityType, array $responseData) {
    $batch = new ImporterCrmBatchExecutable();

    $values = [];
    if ($entityType == 'contact') {
      $values = $this->prepareContactMappingValues($responseData);
    }
    elseif ($entityType == 'company') {
      $values = $this->prepareCompanyMappingValues($responseData);
    }

    $batch->entityImportOperations($entityType, $values);
  }

  /**
   * {@inheritdoc}
   */
  abstract public function prepareContactMappingValues(array $responseData);

  /**
   * {@inheritdoc}
   */
  abstract public function prepareCompanyMappingValues(array $responseData);

  /**
   * {@inheritdoc}
   */
  abstract public function buildProvider();

  /**
   * {@inheritdoc}
   */
  abstract public function buildEndpointPath($entityType = '');

  /**
   * {@inheritdoc}
   */
  public function processAddressData(array $addressData) {
    return [
      'country_code' => !empty($addressData['country_code']) ? $addressData['country_code'] : '',
      'address_line1' => !empty($addressData['address_line1']) ? $addressData['address_line1'] : '',
      'address_line2' => !empty($addressData['address_line2']) ? $addressData['address_line2'] : '',
      'locality' => !empty($addressData['locality']) ? $addressData['locality'] : '',
      'administrative_area' => !empty($addressData['state']) ? $addressData['state'] : '',
      'postal_code' => !empty($addressData['zip_code']) ? $addressData['zip_code'] : '',
    ];
  }

  /**
   * Gets the country code.
   */
  public function getCountryCode($entityCountry) {
    $countries = CountryManager::getStandardList();
    foreach ($countries as $key => $country) {
      if (!empty($entityCountry) && $entityCountry == $country->getUntranslatedString()) {
        return $key;
      }
    }

    return FALSE;
  }

  /**
   * Initializes the data needed for the data retrieve object.
   */
  protected function initializeRetrieveDataCrm() {
    if (!empty($this->requestConfig)) {
      // Build and set the provider.
      $this->retrieveDataCrm->setProvider($this->buildProvider());
      // Set the endpoint information.
      $baseUrl = isset($this->requestConfig['domain']) ? $this->requestConfig['domain'] : '';
      $this->retrieveDataCrm->setEndpointUrl($baseUrl);
      // Set credentials.
      $this->retrieveDataCrm->setCredentials($this->requestConfig);
      // Set default grant type.
      $this->retrieveDataCrm->setGrantType('authorization_code');
    }
  }

  /**
   * Checks if the absolute url is valid.
   */
  public function urlIsValid($url) {
    return UrlHelper::isValid($url, TRUE) ? $url : '';
  }

}
