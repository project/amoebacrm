<?php

namespace Drupal\amoebacrm\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\workflows\Form\WorkflowTransitionAddForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AmoebacrmWorkflowTransitionAddForm.
 */
class AmoebacrmWorkflowTransitionAddForm extends WorkflowTransitionAddForm {

  /**
   * The action plugin manager.
   *
   * @var \Drupal\Core\Action\ActionManager
   */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->manager = $container->get('plugin.manager.action');
    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $actionsConfiguration = $this->entityTypeManager->getStorage('action')->loadByProperties(['type' => 'actions_manager']);
    $existingActions = [];
    foreach ($actionsConfiguration as $id => $actionConfiguration) {
      $existingActions[$id] = $actionConfiguration->label();
    }
    $actions = [];
    foreach ($this->manager->getDefinitionsByType('actions_manager') as $id => $definition) {
      if (is_subclass_of($definition['class'], '\Drupal\Core\Plugin\PluginFormInterface')) {
        $actions[$id] = $definition['label'];
      }
    }

    // @todo: See here how to proceed (not working properly).
    asort($actions);
    if (!empty($existingActions)) {
      $form['configured_actions'] = [
        '#type' => 'details',
        '#title' => $this->t('Choose to trigger an existing action'),
        '#attributes' => ['class' => ['container-inline']],
        '#open' => TRUE,
      ];
      $form['configured_actions']['actions'] = [
        '#type' => 'select',
        '#title' => $this->t('Select an action'),
        '#options' => $existingActions,
        '#multiple' => TRUE,
      ];
    }
    $actions = [];
    foreach ($this->manager->getDefinitionsByType('actions_manager') as $id => $definition) {
      if (is_subclass_of($definition['class'], '\Drupal\Core\Plugin\PluginFormInterface')) {
        $actions[$id] = $definition['label'];
      }
    }

    // @todo: Define new actions form to configure.
    asort($actions);
    if (!empty($actions)) {
      $form['parent'] = [
        '#type' => 'details',
        '#title' => $this->t('Create an advanced action'),
        '#attributes' => ['class' => ['container-inline']],
        '#open' => TRUE,
      ];
      $form['parent']['action'] = [
        '#type' => 'select',
        '#title' => $this->t('Action'),
        '#title_display' => 'invisible',
        '#options' => $actions,
        '#empty_option' => $this->t('- Select -'),
      ];
      $form['parent']['actions'] = [
        '#type' => 'actions',
      ];
      $form['parent']['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Create'),
      ];
    }
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    if ($form_state->getValue('actions') && $form_state->getValue('id')) {
      /* @var \Drupal\workflows\WorkflowInterface $workflow */
      $workflow = $this->getEntity();
      $workflowTypeSettings = $workflow->get('type_settings');
      // Add the actions defined on the type settings
      // config entity section (transition).
      $workflowTypeSettings['transitions'][$form_state->getValue('id')]['actions'][] = $form_state->getValue('actions');
      $workflow->set('type_settings', $workflowTypeSettings);
      $workflow->save();
    }
  }

}
