<?php

namespace Drupal\amoebacrm\Oauth2;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\State\StateInterface;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Base functionality for the connection between AmoebaCrm and external CRMs.
 */
class RetrieveDataCrm implements RetrieveDataCrmInterface {

  /**
   * The config settings name particularity.
   *
   * @var string
   */
  protected $crmType = 'default';

  /**
   * The request grant type for Oauth2.
   *
   * @var string
   */
  protected $grantType;

  /**
   * The request grant type options for Oauth2.
   *
   * @var array
   */
  protected $grantTypeOptions;

  /**
   * The request provider for Oauth2.
   *
   * @var \League\OAuth2\Client\Provider\AbstractProvider
   */
  protected $provider;

  /**
   * The request endpoint url.
   *
   * @var string
   */
  protected $endpointUrl;

  /**
   * The request endpoint path.
   *
   * @var string
   */
  protected $endpointPath;

  /**
   * The JSON serializer service.
   *
   * @var \Drupal\Component\Serialization\Json
   */
  protected $json;

  /**
   * The Request Stack service.
   *
   * @var \Drupal\Component\Serialization\Json
   */
  protected $requestStack;

  /**
   * The state key value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The request credentials.
   *
   * @var array
   */
  protected $credentials;

  /**
   * Constructor for the data retrieve service.
   *
   * @param \Drupal\Component\Serialization\Json $json
   *   The JSON serializer service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(Json $json, RequestStack $requestStack, StateInterface $state) {
    $this->json = $json;
    $this->requestStack = $requestStack;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function setCrmType($crmType) {
    $this->crmType = $crmType;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCrmType() {
    return $this->crmType;
  }

  /**
   * {@inheritdoc}
   */
  public function setCredentials(array $credentials) {
    $this->credentials = $credentials;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getGrantType() {
    return $this->grantType;
  }

  /**
   * {@inheritdoc}
   */
  public function setGrantType($grantType) {
    $this->grantType = $grantType;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getGrantTypeOptions() {
    return $this->grantTypeOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function setGrantTypeOptions(array $grantTypeOptions) {
    $this->grantTypeOptions = $grantTypeOptions;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return $this->provider;
  }

  /**
   * {@inheritdoc}
   */
  public function setProvider(AbstractProvider $provider) {
    $this->provider = $provider;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpointUrl() {
    return $this->endpointUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function setEndpointUrl($url) {
    $this->endpointUrl = $url;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpointPath() {
    return $this->endpointPath;
  }

  /**
   * {@inheritdoc}
   */
  public function setEndpointPath($path) {
    $this->endpointPath = $path;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataFromCrm($method = 'GET', $parameters = []) {
    $code = $this->retrieveDataFromUrl('code');
    $state = $this->retrieveDataFromUrl('state');
    try {
      if ($this->getGrantType() == 'authorization_code') {
        if (empty($code)) {
          // Authorize the client.
          $authorizationUrl = $this->provider->getAuthorizationUrl();
          // Save the state of the provider.
          $this->state->set('oauthState', $this->provider->getState());
          $redirect = new RedirectResponse($authorizationUrl);
          $redirect->send();
          exit();
        }
        // Check given state against previously stored to mitigate CSRF attack.
        elseif (empty($state) || !empty($this->state->get('oauthState')) && $state !== $this->state->get('oauthState')) {
          // Delete the state from storage.
          if (!empty($this->state->get('oauthState'))) {
            $this->state->delete('oauthState');
          }
        }
        $this->setGrantTypeOptions(['code' => $code]);
      }
      $accessToken = $this->requestAccessToken();
      if (!empty($accessToken)) {
        $request = $this->provider->getAuthenticatedRequest($method, $this->getRetrieveUrl(), $accessToken, $parameters);
        if (!empty($request)) {
          return $this->json->decode($this->provider->getResponse($request)->getBody()->getContents());
        }
      }
    }
    catch (\Exception $e) {
      watchdog_exception('SuiteCrmExporter', $e);
    }

    return FALSE;
  }

  /**
   * Retrieves the code from url.
   *
   * @param string $key
   *   A string containing the name of the data.
   *
   * @return string
   *   The string containing the value of the data.
   */
  protected function retrieveDataFromUrl($key) {
    return $this->requestStack->getCurrentRequest()->get($key);
  }

  /**
   * Request an access token based on the grant type.
   */
  protected function requestAccessToken() {
    try {
      return $this->provider->getAccessToken($this->getGrantType(), $this->getGrantTypeOptions());
    }
    catch (IdentityProviderException $e) {
      watchdog_exception('RetrieveDataCrm', $e);
    }
  }

  /**
   * Gets the data retrieve url.
   *
   * @return string
   *   The url used for the data retrieve request.
   */
  protected function getRetrieveUrl() {
    if (UrlHelper::isExternal($this->getEndpointPath())) {
      return $this->getEndpointPath();
    }
    return rtrim($this->getEndpointUrl(), '/') . $this->getEndpointPath();
  }

}
