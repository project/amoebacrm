<?php

namespace Drupal\amoebacrm\Oauth2;

use League\OAuth2\Client\Provider\AbstractProvider;

/**
 * Base functionality for the connection between AmoebaCrm and external CRMs.
 */
interface RetrieveDataCrmInterface {

  /**
   * Gets the grant type.
   *
   * @return null|string
   *   Returns the grant type if set, NULL otherwise.
   */
  public function getGrantType();

  /**
   * Sets the grant type.
   *
   * @param string $grantType
   *   A string representing the grant type. E.g. authorization_code, password.
   *
   * @return RetrieveDataCrmInterface
   *   The Retrieve data crm object.
   */
  public function setGrantType($grantType);

  /**
   * Gets the grant type options.
   *
   * @return null|string
   *   Returns the grant type options if set, NULL otherwise.
   */
  public function getGrantTypeOptions();

  /**
   * Sets the grant type options.
   *
   * @param array $grantTypeOptions
   *   An array contains the grant type options. E.g. username, password, code.
   *
   * @return RetrieveDataCrmInterface
   *   The Retrieve data crm object.
   */
  public function setGrantTypeOptions(array $grantTypeOptions);

  /**
   * Gets the provider.
   *
   * @return null|\League\OAuth2\Client\Provider\AbstractProvider
   *   Returns the provider if set, NULL otherwise.
   */
  public function getProvider();

  /**
   * Sets the provider.
   *
   * @param \League\OAuth2\Client\Provider\AbstractProvider $provider
   *   The provider object.
   *
   * @return RetrieveDataCrmInterface
   *   The Retrieve data crm object.
   */
  public function setProvider(AbstractProvider $provider);

  /**
   * Gets the endpoint URL.
   *
   * @return null|string
   *   Returns the endpoint URL if set, NULL otherwise.
   */
  public function getEndpointUrl();

  /**
   * Sets the endpoint URL.
   *
   * @param string $url
   *   The endpoint url without trailing slash.
   *
   * @return RetrieveDataCrmInterface
   *   The Retrieve data crm object.
   */
  public function setEndpointUrl($url);

  /**
   * Gets the endpoint path.
   *
   * @return null|string
   *   Returns the endpoint path if set, NULL otherwise.
   */
  public function getEndpointPath();

  /**
   * Sets the endpoint path.
   *
   * @param string $path
   *   The endpoint path with a trailing slash as a prefix.
   *
   * @return RetrieveDataCrmInterface
   *   The Retrieve data crm object.
   */
  public function setEndpointPath($path);

  /**
   * Retrieves date from the crm endpoint.
   */
  public function getDataFromCrm();

  /**
   * Sets the request credentials.
   *
   * @param array $credentials
   *   An array containing the stored crm request credentials.
   */
  public function setCredentials(array $credentials);

  /**
   * Sets the CRM type.
   *
   * @param string $crmType
   *   The name of the CRM.
   *
   * @return RetrieveDataCrmInterface
   *   The Retrieve data crm object.
   */
  public function setCrmType($crmType);

  /**
   * Gets the CRM type.
   *
   * @return null|string
   *   Returns the CRM type if set, NULL otherwise.
   */
  public function getCrmType();

}
