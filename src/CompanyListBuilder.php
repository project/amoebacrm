<?php

namespace Drupal\amoebacrm;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Company entities.
 *
 * @ingroup company
 */
class CompanyListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Company ID');
    $header['name'] = $this->t('Name');
    $header['email'] = $this->t('Email');
    $header['address'] = $this->t('Address');
    $header['phone_number'] = $this->t('Phone');
    $header['fax'] = $this->t('Fax');
    $header['website'] = $this->t('Website');
    $header['facebook'] = $this->t('Facebook');
    $header['instagram'] = $this->t('Instagram');
    $header['linkedin'] = $this->t('LinkedIn');
    $header['twitter'] = $this->t('Twitter');
    $header['number_of_employees'] = $this->t('Number of employees');
    $header['annual_revenue'] = $this->t('Annual revenue');
    $header['industry'] = $this->t('Industry');
    $header['description'] = $this->t('Description');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\amoebacrm\Entity\Company */
    $url = Url::fromRoute('entity.company.canonical', ['company' => $entity->id()]);
    $row['id'] = $entity->id();
    $row['name'] = new Link($entity->getName(), $url);
    $row['email'] = $entity->getEmail();
    $row['address'] = !empty($entity->prepareAddressToDisplay()) ? implode(', ', $entity->prepareAddressToDisplay()) : '';
    $row['phone_number'] = $entity->get('phone_number')->value;
    $row['fax'] = $entity->get('fax')->value;
    $row['website'] = $entity->get('website')->value;
    $row['facebook'] = $entity->get('facebook')->value;
    $row['instagram'] = $entity->get('instagram')->value;
    $row['linkedin'] = $entity->get('linkedin')->value;
    $row['twitter'] = $entity->get('twitter')->value;
    $row['number_of_employees'] = $entity->get('number_of_employees')->value;
    $row['annual_revenue'] = $entity->get('annual_revenue')->value;
    $row['industry'] = $entity->getIndustryName();

    $row['description'] = $entity->get('description')->value;
    return $row + parent::buildRow($entity);
  }

}
