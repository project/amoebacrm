<?php

namespace Drupal\amoebacrm;

use Drupal\content_moderation\EntityOperations;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class for reacting to entity events and trigger actions.
 */
class AmoebacrmActionsOperations extends EntityOperations {

  /**
   * {@inheritdoc}
   */
  public function entityPresave(EntityInterface $entity) {
    if (!\Drupal::currentUser()->isAuthenticated()) {
      return;
    }
    if (!$this->moderationInfo->isModeratedEntity($entity)) {
      return;
    }
    if (!empty($entity->get('moderation_state')->getString()) && !$entity->isNew()) {
      $workflow = $this->moderationInfo->getWorkflowForEntity($entity);
      $workflowType = $workflow->getTypePlugin();
      $currentState = $entity->get('moderation_state')->getString();
      $previousState = $entity->original->get('moderation_state')->getString();
      $transition = $workflowType->getTransitionFromStateToState($previousState, $currentState);
      $typeSettings = $workflow->get('type_settings')['transitions'][$transition->id()];
      if (!empty($typeSettings) && !empty($typeSettings['actions'])) {
        /** @var \Drupal\Core\Queue\QueueFactory $queue_factory */
        $queue_factory = \Drupal::service('queue');
        $queue = $queue_factory->get('cron_trigger_action');
        $item = new \stdClass();
        $item->actions = $typeSettings['actions'];
        $item->entity = $entity;
        $queue->createItem($item);
      }
    }
  }

}
