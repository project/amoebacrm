<?php

namespace Drupal\amoebacrm\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;

/**
 * The style plugin for serialized output formats.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "amoeba_serializer",
 *   title = @Translation("Amoeba Serializer"),
 *   help = @Translation("Serialize Amoeba fields."),
 *   display_types = {"data"}
 * )
 */
class AmoebaSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $rows = [];
    // If the Data Entity row plugin is used, this will be an array of entities
    // which will pass through Serializer to one of the registered Normalizers,
    // which will transform it to arrays/scalars. If the Data field row plugin
    // is used, $rows will not contain objects and will pass directly to the
    // Encoder.
    foreach ($this->view->result as $row_index => $row) {
      $this->view->row_index = $row_index;
      $render_value = $this->view->rowPlugin->render($row);
      // Check if address exists in the view results and override the display.
      if (isset($render_value['address'])) {
        $addressDisplay = $row->_entity->prepareAddressToDisplay();
        $render_value['address'] = $addressDisplay;
      }
      $rows[] = $render_value;
    }
    unset($this->view->row_index);

    // Get the content type configured in the display or fallback to the
    // default.
    if ((empty($this->view->live_preview))) {
      $content_type = $this->displayHandler->getContentType();
    }
    else {
      $content_type = !empty($this->options['formats']) ? reset($this->options['formats']) : 'json';
    }

    return $this->serializer->serialize($rows, $content_type, ['views_style_plugin' => $this]);
  }

}
