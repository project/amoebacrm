<?php

namespace Drupal\amoebacrm\Plugin\WorkflowType;

use Drupal\content_moderation\Plugin\WorkflowType\ContentModeration;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Class SalesPipeline used for defining initial state for the workflow.
 */
class SalesPipeline extends ContentModeration {

  /**
   * Gets the initial state of the entity.
   */
  public function getInitialState($entity = NULL) {
    // Override the initial state to not be 'published' or 'draft'.
    if (!($entity instanceof ContentEntityInterface)) {
      throw new \InvalidArgumentException('A content entity object must be supplied.');
    }
    if ($entity instanceof EntityPublishedInterface) {
      return $this->getState($entity->isPublished() && !$entity->isNew() ? 'lead' : 'contact');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'states' => [
        'lead' => [
          'label' => $this->t('Lead'),
          'published' => FALSE,
          'default_revision' => FALSE,
          'weight' => 0,
        ],
        'contact' => [
          'label' => $this->t('Contact'),
          'published' => TRUE,
          'default_revision' => TRUE,
          'weight' => 1,
        ],
      ],
      'transitions' => [
        'lead' => [
          'label' => $this->t('Create New Lead'),
          'to' => 'lead',
          'weight' => 0,
          'from' => [
            'contact',
            'lead',
          ],
        ],
        'contact' => [
          'label' => $this->t('Create New Contact'),
          'to' => 'contact',
          'weight' => 1,
          'from' => [
            'contact',
            'lead',
          ],
        ],
      ],
      'entity_types' => [],
    ];
  }

}
