<?php

namespace Drupal\amoebacrm\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\system\Entity\Action;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base implementation for trigger action queue.
 */
abstract class TriggerAction extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Creates a new NodePublishBase object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node storage.
   */
  public function __construct(EntityStorageInterface $node_storage) {
    $this->nodeStorage = $node_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity.manager')->getStorage('node')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!empty($data->actions)) {
      $actions = Action::loadMultiple($data->actions);
      if (!empty($actions)) {
        foreach ($actions as $action) {
          if ($action instanceof Action) {
            $action->execute([$data->entity]);
          }
        }
      }
    }
  }

}
