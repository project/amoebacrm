<?php

namespace Drupal\amoebacrm\Plugin\QueueWorker;

/**
 * Triggers an action manually whenever the user choose so.
 *
 * @QueueWorker(
 *   id = "cron_trigger_action",
 *   title = @Translation("Manual Trigger Action"),
 *   cron = {"time" = 20}
 * )
 */
class CronTriggerAction extends TriggerAction {

}
