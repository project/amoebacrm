<?php

namespace Drupal\amoebacrm;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Company entity.
 *
 * @todo: Add a general permission that have access to everything.
 *
 * @see \Drupal\amoebacrm\Entity\Company.
 */
class CompanyAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\amoebacrm\Entity\CompanyInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermissions($account, ['view unpublished company entities', 'administer company content'], 'OR');
        }
        return AccessResult::allowedIfHasPermissions($account, ['view published company entities', 'administer company content'], 'OR');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit company entities', 'administer company content'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete company entities', 'administer company content'], 'OR');
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['add company entities', 'administer company content'], 'OR');
  }

}
