<?php

namespace Drupal\amoebacrm\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Checks access for displaying configuration translation page.
 *
 * @ingroup amoebacrm_access
 */
class AmoebaCrmAccessCheck implements AccessInterface {

  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {
    // This custom access check should be used whenever the user is supposed to
    // have at least one of the administer entity permissions.
    return ($account->hasPermission('administer contact entities') || $account->hasPermission('administer company entities')) ? AccessResult::allowed() : AccessResult::forbidden();
  }

}
