<?php

namespace Drupal\amoebacrm;

use Drupal\amoebacrm\Traits\EntityGenerateTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class for processing SuiteCRM data imports.
 */
class ImporterCrmBatchExecutable {
  use StringTranslationTrait;
  use EntityGenerateTrait;

  /**
   * Creates entities from the data received from SuiteCRM.
   */
  public static function importCreateEntities($entity_type, $entities, &$context) {
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($entities);
      $context['results']['import_success'] = !empty($context['results']['import_success']) ? $context['results']['import_success'] : 0;
      $context['results']['update_success'] = !empty($context['results']['update_success']) ? $context['results']['update_success'] : 0;
      $context['results']['import_errors'] = !empty($context['results']['import_errors']) ? $context['results']['import_errors'] : 0;
      $context['results']['update_errors'] = !empty($context['results']['update_errors']) ? $context['results']['update_errors'] : 0;
    }
    $uniqueField = [
      'contact' => 'email',
      'company' => 'name',
    ];
    foreach ($entities as $entity) {
      $entity_id = self::entityLookup($entity_type, $uniqueField[$entity_type], $entity[$uniqueField[$entity_type]]);
      if (!empty($entity_id)) {
        // If the entity already exists, update it.
        if (\Drupal::state()->get('force_update') && self::updateEntity($entity_type, $entity_id, $entity)) {
          $context['results']['update_success'] += 1;
        }
        else {
          $context['results']['update_errors'] += 1;
        }
      }
      else {
        // If the entity does not exists, create it.
        if (self::entityCreate($entity_type, $entity)) {
          $context['results']['import_success'] += 1;
        }
        else {
          $context['results']['import_errors'] += 1;
        }
      }
      // Update our progress information.
      $context['sandbox']['progress']++;
    }

    // Inform the batch engine that we are not finished and provide an
    // estimation of the completion level we reached.
    if ($context['sandbox']['progress'] >= $context['sandbox']['max']) {
      $context['finished'] = ($context['sandbox']['progress'] >= $context['sandbox']['max']);
    }
  }

  /**
   * Batch 'finished' callback used by both batch 1 and batch 2.
   */
  public static function importEntitiesFinished($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    \Drupal::state()->delete('force_update');
    if ($success) {
      if (!empty($results['import_success'])) {
        $messenger->addMessage(t('@count item(s) were successfully imported.', ['@count' => $results['import_success']]));
      }
      if (!empty($results['update_success'])) {
        $messenger->addMessage(t('@count item(s) were successfully updated.', ['@count' => $results['update_success']]));
      }
      if (!empty($results['import_errors'])) {
        $messenger->addError(t('@count item(s) were not imported.', ['@count' => $results['import_errors']]));
      }
      if (!empty($results['update_errors'])) {
        $messenger->addError(t('@count item(s) found but these were not updated because the update checkbox was unchecked.', ['@count' => $results['update_errors']]));
      }
    }
    else {
      $error_operation = reset($operations);
      $messenger->addMessage(
        t('An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

  /**
   * Prepares the batch operations for the import.
   */
  public function entityImportOperations($entity_type, $entities) {
    $operations = [];
    $messenger = \Drupal::messenger();
    if (!empty($entities) && is_array($entities)) {
      $entities_pieces = array_chunk($entities, 10);

      foreach ($entities_pieces as $entities_piece) {
        $operations[] = [
          [get_class($this), 'importCreateEntities'],
          [$entity_type, $entities_piece],
        ];
      }
    }

    $count = count($operations);
    if ($count == 0) {
      return $messenger->addError('There are no entities to import');
    }
    else {
      $batch = [
        'title' => $this->t('Import @entity', ['@entity' => $entity_type]),
        'operations' => $operations,
        'finished' => [get_class($this), 'importEntitiesFinished'],
        'init_message' => $this->t('Import is starting.'),
        'progress_message' => $this->t('Processed @current out of @total.'),
        'error_message' => $this->t('Importing has encountered an error.'),
      ];

      batch_set($batch);
      // This is set for making the batch work on build form.
      $batch =& batch_get();
      $batch['progressive'] = FALSE;
      batch_process();
    }

  }

}
